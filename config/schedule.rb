# every :day, at: ['8am', '9am', '10am', '1pm'] do
#   rake "blast:call"
# end

every :day, at: ['8:05am', '9am', '10am'] do
  rake "blast:first_attempt"
end

every :day, at: ['1pm'] do
  rake "blast:second_attempt"
end

every :day, :at => '6:30 pm' do
 rake "blast:clean_up"
end

every :day, at: '7:30am' do
  rake "sidekiq:start"
end

every :day, at: '8pm' do
  rake "sidekiq:terminate"
end

every "0,10,20,30,40,50 * * * 1,2,3,4,5,6)" do
  rake "sidekiq:status"
end
