require 'sidekiq/web'

Rails.application.routes.draw do

  root to: "home#index"

  controller :home do
    get 'reports' , to: "reports#index"
    get 'reports/selected-options'
    get 'reports/filter-by-params'
    get 'reports/option-selected'
    get 'reports/filter-by-option-selected'
    get 'reports/iamhidden', to: 'reports#sms_sent'
    get 'reports/sms-sent-data'
    get 'reports/sms-received'
    get 'reports/sms-received-data'
    get 'reports/generate_csv'
    get 'reports/generate_report'
    get 'reports/download_messages'
    get 'reports/summary_reports'
  end

  get 'call/index'
  get 'home/index'
  get 'home/release'

  devise_for :users
  authenticate :user, lambda { |u| u.admin? } do
    mount Sidekiq::Web => '/sidekiq'
  end

  post 'upload-data' , to: "home#upload"

  match "globelabs/calls" => "call#start_calling", :via => [:post]
  post "/globelabs/ask",
    to: "call#ask"
  post "/globelabs/ask-answered",
    to: "call#ask_answered"
  post "/globelabs/ask-payment-options",
    to: "call#ask_payment_options"
  post "/globelabs/payment-options-answered",
    to: "call#payment_options_answered"
  post "/globelabs/incomplete-call",
    to: "call#incomplete_call"
  post "/globelabs/incomplete-call-transfer",
    to: "call#incomplete_call_transfer"
  post "/globelabs/incomplete-transfer",
    to: "call#incomplete_transfer"
  post "/globelabs/hangup-transfer",
    to: "call#hangup_transfer"
  post "/globelabs/hangup",
    to: "call#hangup"
  post "/globelabs/receive",
    to: "home#receive"
  post "/globelabs/go-back-main-menu-answered",
    to: "call#go_back_main_menu_answered"
  post "/globelabs/go-back-main-menu-hangup",
    to: "call#go_back_main_menu_hangup"
  post "/globelabs/error",
    to: "call#error"

  get "/globelabs/call",
    to: "call#index"
  get '/reset_accounts',
    to: 'home#reset_accounts'
  get '/start_sidekiq',
    to: 'home#start_sidekiq'

  delete "/globelabs/delete", to: "reports#delete"

  mount API::Root => '/'

  scope :globelabs do
    namespace :v2 do
      post '/start_call', to: 'call#start_calling'
      post '/event', to: 'call#event'
      post '/transfer/event', to: 'call#transfer_event'
      get '/answer', to: 'call#answer'

      post '/answer_action_selected', to: 'call#answer_action_selected'
      post '/payment_option_selected', to: 'call#payment_option_selected'
      post '/repeat_info_selected', to: 'call#repeat_info_selected'
    end
  end
end
