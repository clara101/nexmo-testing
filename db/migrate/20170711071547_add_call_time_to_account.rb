class AddCallTimeToAccount < ActiveRecord::Migration
  def change
    add_column :accounts, :fa_call_time, :datetime
    add_column :accounts, :sa_call_time, :datetime
  end
end
