class AddIndexToOptionSelectedLogs < ActiveRecord::Migration
  def change
    add_index(:option_selected_logs, [:created_at,
                                      :option])
  end
end
