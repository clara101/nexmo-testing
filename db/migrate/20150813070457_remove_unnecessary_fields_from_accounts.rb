class RemoveUnnecessaryFieldsFromAccounts < ActiveRecord::Migration
  def change
    remove_column :accounts, :option_selected, :string
    remove_column :accounts, :fa_is_sms_sent, :boolean
    remove_column :accounts, :fa_send_sms_attempt, :integer
    remove_column :accounts, :sa_is_sms_sent, :boolean
    remove_column :accounts, :sa_send_sms_attempt, :integer
  end
end
