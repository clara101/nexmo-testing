class AddIndexToLogs < ActiveRecord::Migration
  def change
    add_index(:logs, :mobtel)
    add_index(:logs, :process_type)
    add_index(:logs, :status)
    add_index(:logs, :created_at)
  end
end
