class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.string :mobtel
      t.string :account_no
      t.string :amount
      t.date :due_date
      t.string :voice_session_id
      t.string :option_selected
      t.boolean :fa_is_sms_sent
      t.integer :fa_send_sms_attempt
      t.integer :fa_call_status
      t.integer :fa_call_attempt
      t.boolean :sa_is_sms_sent
      t.integer :sa_send_sms_attempt
      t.integer :sa_call_status
      t.integer :sa_call_attempt

      t.timestamps null: false
    end

    add_index :accounts, :mobtel
    add_index :accounts, :voice_session_id
    
  end
end
