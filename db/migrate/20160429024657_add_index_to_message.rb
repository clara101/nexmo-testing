class AddIndexToMessage < ActiveRecord::Migration
  def change
    add_index :messages, :created_at
    add_index :messages, :mobtel
  end
end
