class CreateLogs < ActiveRecord::Migration
  def change
    create_table :logs do |t|
      t.string :mobtel
      t.string :process_type
      t.string :status
      t.string :request_type
      t.text :params

      t.timestamps null: false
    end
  end
end
