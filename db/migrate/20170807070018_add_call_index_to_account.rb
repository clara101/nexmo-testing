class AddCallIndexToAccount < ActiveRecord::Migration
  def change
    add_index :accounts, :fa_call_status
    add_index :accounts, :sa_call_status
  end
end
