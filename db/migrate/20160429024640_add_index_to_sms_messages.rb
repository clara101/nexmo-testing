class AddIndexToSmsMessages < ActiveRecord::Migration
  def change
    add_index(:sms_messages, :created_at)
  end
end
