class AddProcessDateToAccounts < ActiveRecord::Migration
  def change
    add_column :accounts, :process_date, :date
  end
end
