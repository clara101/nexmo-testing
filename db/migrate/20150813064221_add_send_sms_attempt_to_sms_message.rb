class AddSendSmsAttemptToSmsMessage < ActiveRecord::Migration
  def change
    add_column :sms_messages, :send_sms_attempt, :integer
  end
end
