class AddIndexToAccounts < ActiveRecord::Migration
  def change
    add_index(:accounts, :process_date)
    add_index(:accounts, :created_at)
  end
end
