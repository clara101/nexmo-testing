class AddNexmoAttrsToAccounts < ActiveRecord::Migration
  def change
    add_column :accounts, :conversation_uuid, :string
    add_column :accounts, :uuid, :string
  end
end
