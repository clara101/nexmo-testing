class CreateSmsMessages < ActiveRecord::Migration
  def change
    create_table :sms_messages do |t|
      t.text :message
      t.boolean :is_sent
      t.references :account, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
