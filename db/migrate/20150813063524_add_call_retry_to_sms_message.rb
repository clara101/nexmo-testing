class AddCallRetryToSmsMessage < ActiveRecord::Migration
  def change
    add_column :sms_messages, :call_retry, :integer
  end
end
