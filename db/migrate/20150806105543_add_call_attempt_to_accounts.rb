class AddCallAttemptToAccounts < ActiveRecord::Migration
  def change
    add_column :accounts, :call_retry, :integer
  end
end
