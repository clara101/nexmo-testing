# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180221084643) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.string   "mobtel"
    t.string   "account_no"
    t.string   "amount"
    t.date     "due_date"
    t.string   "voice_session_id"
    t.integer  "fa_call_status"
    t.integer  "fa_call_attempt"
    t.integer  "sa_call_status"
    t.integer  "sa_call_attempt"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "call_retry"
    t.date     "process_date"
    t.string   "fname"
    t.string   "my_type"
    t.integer  "account_id"
    t.datetime "fa_call_time"
    t.datetime "sa_call_time"
    t.string   "conversation_uuid"
    t.string   "uuid"
  end

  add_index "accounts", ["created_at"], name: "index_accounts_on_created_at", using: :btree
  add_index "accounts", ["mobtel"], name: "index_accounts_on_mobtel", using: :btree
  add_index "accounts", ["process_date"], name: "index_accounts_on_process_date", using: :btree
  add_index "accounts", ["voice_session_id"], name: "index_accounts_on_voice_session_id", using: :btree

  create_table "logs", force: :cascade do |t|
    t.string   "mobtel"
    t.string   "process_type"
    t.string   "status"
    t.string   "request_type"
    t.text     "params"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "logs", ["created_at"], name: "index_logs_on_created_at", using: :btree
  add_index "logs", ["mobtel"], name: "index_logs_on_mobtel", using: :btree
  add_index "logs", ["process_type"], name: "index_logs_on_process_type", using: :btree
  add_index "logs", ["status"], name: "index_logs_on_status", using: :btree

  create_table "messages", force: :cascade do |t|
    t.text     "message"
    t.string   "mobtel"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "messages", ["created_at"], name: "index_messages_on_created_at", using: :btree
  add_index "messages", ["mobtel"], name: "index_messages_on_mobtel", using: :btree

  create_table "option_selected_logs", force: :cascade do |t|
    t.string   "option"
    t.integer  "account_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "option_selected_logs", ["account_id"], name: "index_option_selected_logs_on_account_id", using: :btree
  add_index "option_selected_logs", ["created_at", "option"], name: "index_option_selected_logs_on_created_at_and_option", using: :btree

  create_table "sms_messages", force: :cascade do |t|
    t.text     "message"
    t.boolean  "is_sent"
    t.integer  "account_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "call_retry"
    t.integer  "send_sms_attempt"
  end

  add_index "sms_messages", ["account_id"], name: "index_sms_messages_on_account_id", using: :btree
  add_index "sms_messages", ["created_at"], name: "index_sms_messages_on_created_at", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",       null: false
    t.string   "encrypted_password",     default: "",       null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,        null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "role",                   default: "client"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "option_selected_logs", "accounts"
  add_foreign_key "sms_messages", "accounts"
end
