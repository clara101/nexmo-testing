module ReportJsonSerializer

	def self.summary_reports_to_json(reports, report_type)
		arr_reports = []

		if reports.present?
			for report in reports

				proc_date =
					case report_type
					when 'daily'
						report.process_date
					when 'weekly'
						week = report.upload.beginning_of_week.to_date
						"Week##{week.strftime("%U").to_i} #{week.strftime('%Y')}"
					when 'monthly'
						date = report.upload.beginning_of_month.to_date
						"#{date.strftime('%B')} #{date.strftime('%Y')}"
					end

					first_attempt_number =
	          report.first_answered +
	          report.first_unanswered +
	          report.first_rejected_or_busy

	        first_contact_rate = ((first_attempt_number.to_f / report.accounts.to_f) * 100).round(1) rescue 0

	        second_attempt_contacted =
	          report.second_answered +
	          report.second_unanswered +
	          report.second_rejected_or_busy

	        second_attempt_number =
	          report.first_unanswered +
	          report.first_rejected_or_busy +
	          report.first_unreachable

	        second_contact_rate = ((second_attempt_contacted.to_f / second_attempt_number.to_f) * 100).round(1) rescue 0

				arr_reports << {
					attempted: report.attempted,
					process_date: proc_date,
					first_answered: report.first_answered,
					first_unanswered: report.first_unanswered,
					first_rejected_or_busy: report.first_rejected_or_busy,
					first_unreachable: report.first_unreachable,
					first_contact_rate: first_contact_rate.to_s == "NaN" ? 0 : first_contact_rate,
					second_answered: report.second_answered,
					second_unanswered: report.second_unanswered,
					second_rejected_or_busy: report.second_rejected_or_busy,
					second_unreachable: report.second_unreachable,
					second_contact_rate: second_contact_rate.to_s == "NaN" ? 0 : second_contact_rate,
					accounts: report.accounts,
					upload: report.upload
				}
			end
		end

		{ data: arr_reports,
			pagination: {
				page_total: reports.total_pages,
				per_page: reports.default_per_page,
				total_items: reports.total_count
			}
		}
	end

	def self.summary_to_json reports

		arr_reports = Array.new

		if reports.present?
			for report in reports
				arr_reports << {
          process_date: report.process_date,
          upload: report.upload,
          process_account_total: report.process_account_total,
          fa_answered_total: report.fa_answered,
          fa_unanswered_total: report.fa_unanswered,
          fa_unreacheable_total: report.fa_unreacheable,
          fa_rejected_or_busy_total: report.fa_rejected_or_busy,
          second_retry_process_account_total: (report.fa_unanswered + report.fa_rejected_or_busy + report.fa_unreacheable),
          sa_answered_total: report.sa_answered,
          sa_unanswered_total: report.sa_unanswered,
          sa_unreacheable_total: report.sa_unreacheable,
          sa_rejected_or_busy_total: report.sa_rejected_or_busy,
          first_attempt: report.first_attempt
        }
      end
    end

    {
      data: arr_reports ,
      pagination: {
        page_total: reports.total_pages,
        per_page: reports.default_per_page,
        total_items: reports.total_count
      }
    }
	end

	def self.option_selected_to_json reports

		arr_reports = Array.new

    if reports.present?
			for report in reports
				arr_reports << {
          process_account_total: report.process_account_total,
          process_date: report.process_date,
          incomplete_call_transfer: report.incomplete_call_transfer,
          option_one: report.option_one,
          option_two: report.option_two,
          option_three: report.option_three,
          option_four: report.option_four,
          option_five: report.option_five,
          option_six: report.option_six,
          option_seven: report.option_seven,
          option_eight: report.option_eight,
          option_nine: report.option_nine,
          option_ten: report.option_ten
				}
			end
		end

		{ data: arr_reports , pagination: { page_total: reports.total_pages, per_page: reports.default_per_page, total_items: reports.total_count } }

	end

	def self.sms_sent_to_json reports

		arr_reports = Array.new

		if reports.present?
			for report in reports
				arr_reports << {
					process_date: report.process_date,
					sms_sent_total: report.sms_sent_total
				}
			end
		end

		{ data: arr_reports , pagination: { page_total: reports.total_pages, per_page: reports.default_per_page, total_items: reports.total_count } }

	end

	def self.sms_received_to_json reports

		arr_reports = Array.new

		if reports.present?
			for report in reports
				arr_reports << {
					process_date: report.process_date,
					sms_total: report.sms_total
				}
			end
		end

		{ data: arr_reports , pagination: { page_total: reports.total_pages, per_page: reports.default_per_page, total_items: reports.total_count } }

	end

end
