class GlobelabsSmsService



	def self.send(json, account, call_attempt)
		GlobelabsApiSmsWorker.perform_async(GlobalConstants::APP_ID, GlobalConstants::APP_SECRET, GlobalConstants::SHORT_CODE, json, account, call_attempt)
	end

end