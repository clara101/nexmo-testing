require 'message_helper'

class GlobelabsCallService
  class << self
    include ApplicationHelper

    def call(params)
      end_call = false
      session_id = params[:session][:id]
      call_retry = params[:session][:parameters][:call_retry].to_i
      account_id = params[:session][:parameters][:account_id]

      account = Account.find(account_id)
      end_call = true if (account.fa_call_status.eql?(4) || account.sa_call_status.eql?(4))

      if end_call.eql? false
        if call_retry == 1
          if account.fa_call_attempt.nil? || account.fa_call_attempt == 0
            account.fa_call_attempt = 1
            account.fa_call_time = DateTime.now
            account.call_retry = call_retry
            account.fa_call_status = Account::ON_PROGRESS
          end
        elsif call_retry == 2
          if account.sa_call_attempt.nil? || account.sa_call_attempt == 0
            account.sa_call_attempt = 1
            account.sa_call_time = DateTime.now
            account.call_retry = call_retry
            account.sa_call_status = Account::ON_PROGRESS
          end
        end
        account.save!

        Log.create(mobtel: account.mobtel,
                   process_type: Log::PROCESS_TYPE_REQUEST,
                   request_type: Log::REQUEST_TYPE_CALL,
                   status: Log::STATUS_PROCESS,
                   params: "#{params.to_json}")

        t = Tropo::Generator.new
        t.call to: account.decrypted_mobtel, from: GlobalConstants::FROM_CALLER_ID
        t.say "#{GlobalConstants::APP_ADDRESS}intro1.0.mp3"
        t.say account.fname
        t.on event: 'continue', next: "/globelabs/ask?fname=#{account.fname.gsub(' ','_')}&acc=#{account_id}"
        t.on event: 'hangup', next: "/globelabs/hangup?acc=#{account_id}"
        t.on event: 'incomplete', next: "/globelabs/incomplete-call?acc=#{account_id}"
        t.on event: 'error', next: "/globelabs/error?msisdn=#{account.decrypted_mobtel}&acc=#{account_id}"
        t.response
      end
    end

    def ask(params, acc)
      # acc_id = "#{decrypt_account(acc)}"
      t = Tropo::Generator.new
      t.say "#{GlobalConstants::APP_ADDRESS}intro2.0.mp3"
      t.say "#{params[:fname].gsub('_',' ')}"
      t.ask name: 'response_code',
      timeout: GlobalConstants::ANSWER_TIMEOUT,
          say: { value: "#{GlobalConstants::APP_ADDRESS}intro2.1.mp3 #{params[:fname].gsub('_',' ')} #{GlobalConstants::APP_ADDRESS}intro2.2.mp3" },
          choices: { value: "1", mode: "dtmf", terminator: "#"}
      t.on event: 'continue', next: "/globelabs/ask-answered?acc=#{acc}"
      t.on event: 'hangup', next: "/globelabs/hangup?acc=#{acc}"
      t.response
    end

    def ask_payment_options(params, acc)
      # encrypted_id = "#{acc}"
      # decrypted_acc = decrypt_account(acc)
      # acc_id = "#{decrypted_acc}"
      t = Tropo::Generator.new
      self.ask_payment_options_data(t, acc)
      t.on event: 'hangup', next: "/globelabs/hangup?acc=#{acc}"
      t.response
    end

    def ask_answered(params, acc)
      # encrypted_id = "acc=#{acc}"
      # acc_id = "acc=#{decrypt_account(acc)}"
      disposition = params[:result][:actions][:response_code][:disposition]
      account = find_account(acc)

      if disposition.downcase == "success"
        answer = params[:result][:actions][:response_code][:value]
        # message_data = { amount: account.amount, due_date: account.due_date }

        t = Tropo::Generator.new
        case answer.to_i
        when 1
          t.say "#{GlobalConstants::APP_ADDRESS}intro3.0.mp3"
          t.say "<?xml version='1.0'?><speak><say-as interpret-as='vxml:number'>#{account.amount}</say-as> pesos.</speak>"
          t.say "#{GlobalConstants::APP_ADDRESS}intro3.1.mp3"
          t.say "<?xml version='1.0'?><speak><say-as interpret-as='vxml:digits'>#{account.account_no}</say-as>.</speak>"
          t.say "#{GlobalConstants::APP_ADDRESS}intro3.2.mp3"
          t.on event: 'continue', next: "/globelabs/ask-payment-options?acc=#{acc}"
        end

        unless answer.to_i == 1
          if account.call_retry == 1
            account.fa_call_status = Account::ANSWERED
          elsif account.call_retry == 2
            account.sa_call_status = Account::ANSWERED
          end
        end
        account.save

        t.on event: 'hangup', next: "/globelabs/hangup?acc=#{acc}"
        t.response
      else
        hangup(params, account.id)
      end
    end

    def ask_payment_options_data(t, encrypted_id)
      t.ask name: 'response_code',
      timeout: GlobalConstants::ANSWER_TIMEOUT,
          say: { value: "#{GlobalConstants::APP_ADDRESS}options.mp3" },
          choices: { value: "1, 2, 3, 4, 5, 6, 7", mode: "dtmf", terminator: "#"}
      t.on event: 'continue', next: "/globelabs/payment-options-answered?acc=#{encrypted_id}"
      t.on event: 'hangup', next: "/globelabs/hangup?acc=#{encrypted_id}"
    end

    def ask_go_back_main_menu(t, option, encrypted_id, account_id)
      encrypted_id = "#{account_id}"
      t.ask name: 'response_code',
          timeout: GlobalConstants::ANSWER_TIMEOUT,
          say: { value: "#{GlobalConstants::APP_ADDRESS}options_again.mp3"},
          choices: { value: "1, 9", mode: "dtmf", terminator: "#"}
      t.on event: 'continue', next: "/globelabs/go-back-main-menu-answered?option_selected=#{option}&acc=#{encrypted_id}"
      t.on event: 'hangup', next: "/globelabs/go-back-main-menu-hangup?acc=#{encrypted_id}"
    end

    def hangup(params, id)
      account = Account.find(id)
      if account.call_retry == 1
        account.fa_call_status = Account::ANSWERED
        send_sms(account, MessageHelper.message({amount:account.amount, account: account.account_no, fname: account.fname}))
      elsif account.call_retry == 2
        account.sa_call_status = Account::ANSWERED
      end

      account.option_selected_logs.create(option: '10')
      account.save

      Log.create(mobtel: account.mobtel,
                 process_type: Log::PROCESS_TYPE_RESPONSE,
                 request_type: Log::REQUEST_TYPE_CALL,
                 status: Log::STATUS_SUCESS,
                 params: "#{params.to_json}")
    end

    def payment_options_answered(params, acc)
      disposition = params[:result][:actions][:response_code][:disposition]
      account = find_account(acc)
      encrypted_id = "#{acc}"

      if disposition.downcase == "success"

        answer = params[:result][:actions][:response_code][:value]
        message_data = { amount: account.amount, due_date: account.due_date }

        t = Tropo::Generator.new
        case answer.to_i
        when 1
          send_sms(account, MessageHelper.sms_one)
          t.say "#{GlobalConstants::APP_ADDRESS}#{account.my_type.downcase}_option_one.mp3"
          self.ask_go_back_main_menu(t, answer, encrypted_id, account.id)
          # t.say "#{GlobalConstants::APP_ADDRESS}nega_option_one.mp3"
          # send_sms(account, MessageHelper.sms_nega_one)
          # self.ask_payment_options_data(t)
        when 2
          t.say "#{GlobalConstants::APP_ADDRESS}#{account.my_type.downcase}_option_two.mp3"
          self.ask_go_back_main_menu(t, answer, encrypted_id, account.id)
          send_sms(account, MessageHelper.sms_two)
          # t.say "#{GlobalConstants::APP_ADDRESS}nega_option_two.mp3"
          # send_sms(account, MessageHelper.sms_nega_two)
          # self.ask_payment_options_data(t)
        when 3
          t.say "#{GlobalConstants::APP_ADDRESS}option_three.mp3"
          send_sms(account, MessageHelper.sms_three)
          self.ask_go_back_main_menu(t, answer, encrypted_id, account.id)
          # t.say "#{GlobalConstants::APP_ADDRESS}nega_option_three.mp3"
          # send_sms(account, MessageHelper.sms_nega_three)
          # self.ask_payment_options_data(t)
        when 4
          t.say "#{GlobalConstants::APP_ADDRESS}#{account.my_type.downcase}_option_four.mp3"
          send_sms(account, MessageHelper.sms_four)
          self.ask_go_back_main_menu(t, answer, encrypted_id, account.id)
          # t.say "#{GlobalConstants::APP_ADDRESS}nega_option_four.mp3"
          # send_sms(account, MessageHelper.sms_nega_four)
          # self.ask_payment_options_data(t)
        when 5
          t.say "#{GlobalConstants::APP_ADDRESS}#{account.my_type.downcase}_option_five.mp3"
          send_sms(account, MessageHelper.sms_five)
          self.ask_go_back_main_menu(t, answer, encrypted_id, account.id)
          # t.say "#{GlobalConstants::APP_ADDRESS}nega_option_five.mp3"
          # send_sms(account, MessageHelper.sms_nega_five)
          # self.ask_payment_options_data(t)
        when 6
          t.transfer( to: [ GlobalConstants::TRANSFER_TO, GlobalConstants::TRANSFER_TO_WITH_SIP ],
            choices: { terminator: "#"},
            from: "sip:#{account.decrypted_mobtel}@sip.tropo.net",
            on: {
              event: "ring",
              say: { value: "http://onholdsamples.com/samples/music/IN-110.mp3"}
              })
          t.on :event => 'incomplete', :next => "/globelabs/incomplete-transfer?acc=#{encrypted_id}"
          t.on :event => 'hangup', :next => "/globelabs/hangup-transfer?acc=#{encrypted_id}"
        when 7
          self.ask_payment_options_data(t, encrypted_id)
        end

        unless answer.to_i == 7
          if account.call_retry == 1
            account.fa_call_status = Account::ANSWERED
          elsif account.call_retry == 2
            account.sa_call_status = Account::ANSWERED
          end
        end

        account.save

        account.option_selected_logs.create(option: answer) if !answer.to_i.eql?(7)

        t.response
      else
        hangup(params, account.id)
        {}
      end
    end

    def go_back_main_menu_answered(params, option_selected, acc)
      encrypted_id = "#{acc}"
      disposition = params[:result][:actions][:response_code][:disposition]
      account = find_account(acc)

      if disposition.downcase == "success"
        answer = params[:result][:actions][:response_code][:value]

        t = Tropo::Generator.new

        case answer.to_i
        when 1
          my_type = account.my_type.downcase
          address = GlobalConstants::APP_ADDRESS
          case option_selected.to_i
          when 1
            t.say "#{address}#{my_type}_option_one.mp3"
          when 2
            t.say "#{address}#{my_type}_option_two.mp3"
          when 3
            t.say "#{address}option_three.mp3"
          when 4
            t.say "#{address}#{my_type}_option_four.mp3"
          when 5
            t.say "#{address}#{my_type}_option_five.mp3"
          end

          self.ask_go_back_main_menu(t, option_selected, encrypted_id, account.id)

        when 9
          self.ask_payment_options_data(t, encrypted_id)
        end

        t.response
      else
        {}
      end

    end

    def incomplete_call_transfer(params, acc)
      account = find_account(acc)
      account.option_selected_logs.create(option: '0')
      # send_sms(account, MessageHelper.message({amount:account.amount, account: account.account_no}))
    end
    def incomplete_transfer(params, acc)
      account = find_account(acc)
      account.option_selected_logs.create(option: '9')
      # send_sms(account, MessageHelper.message({amount:account.amount, account: account.account_no}))
    end
    def hangup_transfer(params, acc)
      account = find_account(acc)
      account.option_selected_logs.create(option: '8')
      # send_sms(account, MessageHelper.answered_call({account_no: account.account_no, amount: account.amount}))
    end

    def validate_incomplete_call(params, acc)
      error = params[:result][:error]
      account = find_account(acc)

      if error.downcase.include? "outbound call is busy here"
        if account.call_retry == 1
          account.fa_call_status = Account::REJECTED_AND_BUSY
          send_sms(account, MessageHelper.message({amount:account.amount, account: account.account_no, fname: account.fname}))
        elsif account.call_retry == 2
          account.sa_call_status = Account::REJECTED_AND_BUSY
        end

        Log.create(mobtel: account.mobtel,
                   process_type: Log::PROCESS_TYPE_RESPONSE,
                   request_type: Log::REQUEST_TYPE_CALL,
                   status: Log::STATUS_REJECTED_AND_BUSY,
                   params: "#{params.to_json}")

      elsif error.downcase.include? "outbound call has timed out"
        if account.call_retry == 1
          account.fa_call_status = Account::MISSED_CALL
          send_sms(account, MessageHelper.message({amount:account.amount, account: account.account_no, fname: account.fname}))
        elsif account.call_retry == 2
          account.sa_call_status = Account::MISSED_CALL
        end

        Log.create(mobtel: account.mobtel,
                   process_type: Log::PROCESS_TYPE_RESPONSE,
                   request_type: Log::REQUEST_TYPE_CALL,
                   status: Log::STATUS_MISSED_CALL,
                   params: "#{params.to_json}")

      elsif error.downcase.include? "outbound call can not complete"

        Log.create(mobtel: account.mobtel,
                   process_type: Log::PROCESS_TYPE_RESPONSE,
                   request_type: Log::REQUEST_TYPE_CALL,
                   status: Log::STATUS_FAILED,
                   params: "#{params.to_json}")

        column = account.call_retry.eql?(1) ? "fa_call_status=" : "sa_call_status="
        account.send(column, Account::UNREACHABLE)
      end

      account.save
    end

    def send_sms(account, msg)
      if account.decrypted_mobtel.length > 9
        sms = account.sms_messages.create!(message: msg, call_retry: account.call_retry, send_sms_attempt: 1)
        GlobelabsApiSmsWorker.perform_in((Time.now + 15.seconds),
                                         {
                                           message: msg
                                         },
                                         sms.id,
                                         account.id) if sms.present?
      end
    end
  end
end
