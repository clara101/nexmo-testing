class OptionSelectedLog < ActiveRecord::Base
  belongs_to :account

  def self.summary(date)
	self.select("date(created_at) AS process_date, 
		COUNT(DISTINCT account_id) AS process_account_total,
		COALESCE(sum(CASE WHEN option='0' THEN 1 ELSE 0 END),0) AS incomplete_call_transfer,
		COALESCE(sum(CASE WHEN option='1' THEN 1 ELSE 0 END),0) AS option_one,
		COALESCE(sum(CASE WHEN option='2' THEN 1 ELSE 0 END),0) AS option_two,
		COALESCE(sum(CASE WHEN option='3' THEN 1 ELSE 0 END),0) AS option_three,
		COALESCE(sum(CASE WHEN option='4' THEN 1 ELSE 0 END),0) AS option_four,
		COALESCE(sum(CASE WHEN option='5' THEN 1 ELSE 0 END),0) AS option_five,
		COALESCE(sum(CASE WHEN option='6' THEN 1 ELSE 0 END),0) AS option_six,
		COALESCE(sum(CASE WHEN option='7' THEN 1 ELSE 0 END),0) AS option_seven,
		COALESCE(sum(CASE WHEN option='8' THEN 1 ELSE 0 END),0) AS option_eight,
		COALESCE(sum(CASE WHEN option='9' THEN 1 ELSE 0 END),0) AS option_nine,
		COALESCE(sum(CASE WHEN option='10' THEN 1 ELSE 0 END),0) AS option_ten").where(created_at: date).group("date(created_at)").order("date(created_at) DESC")
	end
end