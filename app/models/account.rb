require 'csv'

class Account < ActiveRecord::Base

  has_many :option_selected_logs
  has_many :sms_messages

  #Status
  PENDING = 0
  ON_PROGRESS = 1
  REJECTED_AND_BUSY = 2
  UNREACHABLE = 3
  ANSWERED = 4
  MISSED_CALL = 5
  FAIL_TRANSFER = 6
  TRANSFERED = 7

  PROCESSED = [ANSWERED, REJECTED_AND_BUSY, UNREACHABLE, MISSED_CALL]

  ATTR_DOWNLOADABLES = %w{mobtel updated_at}.freeze

  # Nexmo Statuses
  CALL_STATUS = {
   pending: 0,
   started: 1,
   busy: 2,
   timeout: 3,
   answered: 4,
   unanswered: 5,
   completed: 7,
   ringing: 8,
  }.with_indifferent_access

  scope :for_tomorrow, -> { where(process_date: Date.tomorrow) }
  scope :for_today, -> { where(process_date: Date.today) }
  scope :for_yesterday, -> { where(process_date: Date.yesterday) }

  # before_create do
  #   self.fa_call_status = PENDING
  # end

  ## Start Class Methods
  class << self
    def validate_mobtel_per_day(mobtel,time, encrypted_mobtel)
      !mobtel.eql?('0') &&
      !Account.where(mobtel: encrypted_mobtel,
                     process_date: time.to_date).exists? &&
      !mobtel.eql?("") &&
      !mobtel.to_i.eql?(0) &&
      mobtel.to_i.to_s.eql?(mobtel) &&
      !(mobtel =~ /^[\d]+(\.[\d]+){0,1}$/).nil?
    end

    def summary(date)
      select("process_date, COUNT(mobtel) AS process_account_total,
      COALESCE(sum(CASE WHEN fa_call_attempt=1 THEN 1 ELSE 0 END),0) AS first_attempt,
      COALESCE(sum(CASE WHEN fa_call_status=4 THEN 1 ELSE 0 END),0) AS fa_answered,
      COALESCE(sum(CASE WHEN fa_call_status=5 THEN 1 ELSE 0 END),0) AS fa_unanswered,
      COALESCE(sum(CASE WHEN fa_call_status=3 THEN 1 ELSE 0 END),0) AS fa_unreacheable,
      COALESCE(sum(CASE WHEN fa_call_status=2 THEN 1 ELSE 0 END),0) AS fa_rejected_or_busy,
      COALESCE(sum(CASE WHEN sa_call_status=4 THEN 1 ELSE 0 END),0) AS sa_answered,
      COALESCE(sum(CASE WHEN sa_call_status=5 THEN 1 ELSE 0 END),0) AS sa_unanswered,
      COALESCE(sum(CASE WHEN sa_call_status=3 THEN 1 ELSE 0 END),0) AS sa_unreacheable,
      COALESCE(sum(CASE WHEN sa_call_status=2 THEN 1 ELSE 0 END),0) AS sa_rejected_or_busy, MIN(created_at) AS upload").where(process_date: date).group("process_date").order("process_date DESC")
    end

    def self.option_selected(date)
      select("process_date, COUNT(mobtel) AS process_account_total,
      COALESCE(sum(CASE WHEN fa_call_status=4 THEN 1 ELSE 0 END),0) AS fa_answered,
      COALESCE(sum(CASE WHEN sa_call_status=4 THEN 1 ELSE 0 END),0) AS sa_answered,
      COALESCE(sum(CASE WHEN option_selected='1' THEN 1 ELSE 0 END),0) AS option_selected_one,
      COALESCE(sum(CASE WHEN option_selected='2' THEN 1 ELSE 0 END),0) AS option_selected_two,
      COALESCE(sum(CASE WHEN option_selected='3' THEN 1 ELSE 0 END),0) AS option_selected_three,
      COALESCE(sum(CASE WHEN option_selected='4' THEN 1 ELSE 0 END),0) AS option_selected_four,
      COALESCE(sum(CASE WHEN option_selected='5' THEN 1 ELSE 0 END),0) AS option_selected_five,
      COALESCE(sum(CASE WHEN option_selected='6' THEN 1 ELSE 0 END),0) AS option_selected_six,
      COALESCE(sum(CASE WHEN option_selected='7' THEN 1 ELSE 0 END),0) AS option_selected_seven,
      COALESCE(sum(CASE WHEN option_selected='8' THEN 1 ELSE 0 END),0) AS option_selected_eight,
      COALESCE(sum(CASE WHEN option_selected='9' THEN 1 ELSE 0 END),0) AS option_selected_nine,
      COALESCE(sum(CASE WHEN option_selected='10' THEN 1 ELSE 0 END),0) AS option_selected_ten").where(process_date: date).group("process_date").order("process_date DESC")
    end

    def upload_accounts(params)
      last_mobtel, last_type = "none", ""
      time = params[:blastingDate]
      numbers = []

      CSV.foreach(params[:file].path, headers: false, encoding: 'ISO-8859-1') do |row|
        encrypted_mobtel = Encryptor.encrypt(row[0])
        if Account.validate_mobtel_per_day(row[0], time.to_time, encrypted_mobtel) &&
           ['landline', 'mobile'].include?(row[4].downcase) &&
           (['8','9','10','11','12'].include? row[0].size.to_s)
          Account.create!(mobtel: encrypted_mobtel,
                          account_no: row[1],
                          amount: row[2],
                          fname: row[3],
                          my_type: row[4],
                          process_date: time.to_date)
          numbers << encrypted_mobtel
        else
          last_mobtel, last_type = row[0], row[4]
          Account.delete_all(process_date:time, mobtel: numbers)
          break
        end
      end
      return last_mobtel, last_type
    rescue => e
      Account.delete_all(process_date:time, mobtel: numbers)
      return last_mobtel, last_type
    end

    def to_csv(options = {})
      CSV.generate(headers: false) do |csv|
        all.each do |account|
          if account.mobtel.size > 10
            csv << [account.decrypted_mobtel, account.updated_at]
          else
            csv << ATTR_DOWNLOADABLES.map{ |attr| account.send(attr) }
          end
        end
      end
    end

    def query
      "process_date,
      COUNT(mobtel) AS accounts,
      COALESCE(sum(CASE WHEN fa_call_attempt=1 THEN 1 ELSE 0 END),0) AS attempted,
      COALESCE(sum(CASE WHEN fa_call_status=4 OR fa_call_status=6 THEN 1 ELSE 0 END),0) AS first_answered,
      COALESCE(sum(CASE WHEN fa_call_status=5 THEN 1 ELSE 0 END),0) AS first_unanswered,
      COALESCE(sum(CASE WHEN fa_call_status=2 THEN 1 ELSE 0 END),0) AS first_rejected_or_busy,
      COALESCE(sum(CASE WHEN fa_call_status=3 THEN 1 ELSE 0 END),0) AS first_unreachable,
      COALESCE(sum(CASE WHEN sa_call_status=4 OR sa_call_status=6 THEN 1 ELSE 0 END),0) AS second_answered,
      COALESCE(sum(CASE WHEN sa_call_status=5 THEN 1 ELSE 0 END),0) AS second_unanswered,
      COALESCE(sum(CASE WHEN sa_call_status=2 THEN 1 ELSE 0 END),0) AS second_rejected_or_busy,
      COALESCE(sum(CASE WHEN sa_call_status=3 THEN 1 ELSE 0 END),0) AS second_unreachable,
      MIN(created_at) AS upload"
    end

    def summary(date)
      select(query)
      .where(process_date: date)
      .group("process_date")
      .order("process_date DESC")
    end

    def summary_weekly(date)
      query_week =
        "COUNT(mobtel) AS accounts,
        COALESCE(sum(CASE WHEN fa_call_attempt=1 THEN 1 ELSE 0 END),0) AS attempted,
        COALESCE(sum(CASE WHEN fa_call_status=4 OR fa_call_status=6 THEN 1 ELSE 0 END),0) AS first_answered,
        COALESCE(sum(CASE WHEN fa_call_status=5 THEN 1 ELSE 0 END),0) AS first_unanswered,
        COALESCE(sum(CASE WHEN fa_call_status=2 THEN 1 ELSE 0 END),0) AS first_rejected_or_busy,
        COALESCE(sum(CASE WHEN fa_call_status=3 THEN 1 ELSE 0 END),0) AS first_unreachable,
        COALESCE(sum(CASE WHEN sa_call_status=4 OR sa_call_status=6 THEN 1 ELSE 0 END),0) AS second_answered,
        COALESCE(sum(CASE WHEN sa_call_status=5 THEN 1 ELSE 0 END),0) AS second_unanswered,
        COALESCE(sum(CASE WHEN sa_call_status=2 THEN 1 ELSE 0 END),0) AS second_rejected_or_busy,
        COALESCE(sum(CASE WHEN sa_call_status=3 THEN 1 ELSE 0 END),0) AS second_unreachable,
        MIN(process_date) AS upload"

      select(query_week)
      .where(process_date: date)
      .group("DATE_TRUNC('week', process_date)")
      .order("DATE_TRUNC('week', process_date) DESC")
    end

    def summary_monthly(month, year)
      query_month =
        "COUNT(mobtel) AS accounts,"\
        "MIN(process_date) AS upload,"\
        "COALESCE(sum(CASE WHEN fa_call_attempt=1 THEN 1 ELSE 0 END),0) AS attempted,"\
        "COALESCE(sum(CASE WHEN fa_call_status=4 OR fa_call_status=6 THEN 1 ELSE 0 END),0) AS first_answered,"\
        "COALESCE(sum(CASE WHEN fa_call_status=5 THEN 1 ELSE 0 END),0) AS first_unanswered,"\
        "COALESCE(sum(CASE WHEN fa_call_status=2 THEN 1 ELSE 0 END),0) AS first_rejected_or_busy,"\
        "COALESCE(sum(CASE WHEN fa_call_status=3 THEN 1 ELSE 0 END),0) AS first_unreachable,"\
        "COALESCE(sum(CASE WHEN sa_call_status=4 OR sa_call_status=6 THEN 1 ELSE 0 END),0) AS second_answered,"\
        "COALESCE(sum(CASE WHEN sa_call_status=5 THEN 1 ELSE 0 END),0) AS second_unanswered,"\
        "COALESCE(sum(CASE WHEN sa_call_status=2 THEN 1 ELSE 0 END),0) AS second_rejected_or_busy,"\
        "COALESCE(sum(CASE WHEN sa_call_status=3 THEN 1 ELSE 0 END),0) AS second_unreachable"
      query_where =
        "EXTRACT(MONTH FROM process_date) = #{month} AND "\
        "EXTRACT(YEAR FROM process_date) = #{year}"
      select(query_month)
      .where(query_where)
      .group("DATE_TRUNC('month', process_date)")
    end

    def report_csv(date_from, date_to, report_type)
      date_range =
        case report_type
        when "daily"
          date_from.beginning_of_day..date_to.end_of_day
        when "weekly"
          date_from.beginning_of_week..date_to.end_of_week
        when "monthly"
          date_from.beginning_of_month..date_to.end_of_month
        end
      Account
      .includes(:option_selected_logs)
      .select("accounts.id", "accounts.fa_call_status", "accounts.fa_call_time", "accounts.sa_call_status", "accounts.sa_call_time", "accounts.process_date", "accounts.mobtel")
      .where(process_date: date_range)
      .order('process_date')
    end

    def account_status(account, attempt)
      att = attempt.eql?(1) ? "fa" : "sa"
      status = account.send("#{att}_call_status").to_i

      case status
      when 1 then 'In Progress'
      when 2 then 'Rejected/Busy'
      when 3 then 'Unreachable'
      when 4 then 'Answered'
      when 5 then 'Missed Call'
      when 6 then 'Complete Transfer'
      when 7 then 'Incomplete Transfer'
      else 'N/A'
      end
    end

    def selected_options(account)
      if account.option_selected_logs.pluck(:option).join(',').present?
        options = account.option_selected_logs.pluck(:option)
        options.delete('10')
        options.blank? ? "None" : options.join(',')
      else
        'None'
      end
    end

    def to_report_download
      CSV.generate(headers: true) do |csv|
        csv << [
                 'Mobile Number',
                 'First Call Status',
                 'DateTime of Call',
                 'Second Call Status',
                 'DateTime of Call',
                 'Option/s Availed'
               ]
        all.each do |account|
          Rails.logger.info ">>>>>> #{account.inspect}"
          options = selected_options(account)
          Rails.logger.info ">>>>>> #{options.inspect}"
          fa_status = account_status(account, 1)
          sa_status = account_status(account, 2)
          csv << [
                   account.decrypted_mobtel,
                   fa_status,
                   (account.fa_call_time || "N/A"),
                   sa_status,
                   (account.sa_call_time || "N/A"),
                   options
                 ]
        end
      end
    end

    def summary_reports_call(opts)
      date_from = opts[:date_from]
      date_to = opts[:date_to]
      report_type = opts[:report_type]
      month_year = opts[:selected_month]

      case report_type
      when "daily"
        summary(date_from..date_to)
      when "weekly"
        start_date = date_from.beginning_of_week
        end_date = date_to.end_of_week
        summary_weekly(start_date..end_date)
      when "monthly"
        month = month_year.to_date.strftime('%m')
        year = month_year.to_date.strftime('%Y')
        response = summary_monthly(month, year)

        Rails.logger.info { ">>>>>>>>>>>>>> #{response.inspect}" }
        response
      end
    end
  end
  ## End Class Methods

  def decrypted_mobtel
    Encryptor.decrypt(mobtel)
  end
end
