require 'message_helper'

class GlobelabsV2CallService
  attr_accessor :client

  TIMEOUT = '10'
  TIMEOUT_TRANSFER = '45'

  def initialize
    client
  end

  def client
    @client ||= Nexmo::Client.new(key: GlobalConstants::NEXMO_KEY,
                                secret: GlobalConstants::NEXMO_SECRET,
                                application_id: GlobalConstants::NEXMO_APP_ID,
                                private_key: GlobalConstants::NEXMO_PRIVATE_KEY)
  end

  def call(number)
    client.create_call({
      from: {type: 'phone', number: '917214'},
      to: [{type: 'phone', number: number}],
      answer_url: ["#{GlobalConstants::V2_ADDRESS}/answer"]
    })
  end

  def self.answer(account)
    [
      {
        action: 'stream',
        streamUrl: ["#{GlobalConstants::MP3_ADDRESS}/intro1.0.mp3"]
      },{
        action: 'talk',
        voiceName: 'Naja',
        level: 1,
        text: account.fname
      },{
        action: 'stream',
        streamUrl: ["#{GlobalConstants::MP3_ADDRESS}/intro2.0.mp3"],
        bargeIn: true
      },{
        action: 'talk',
        voiceName: 'Jennifer',
        level: 1,
        text: account.fname
      },{
        action: 'stream',
        streamUrl: ["#{GlobalConstants::MP3_ADDRESS}/intro2.1.mp3"]
      },{
        action: 'talk',
        voiceName: 'Jennifer',
        level: 1,
        text: account.fname
      },{
        action: 'stream',
        streamUrl: ["#{GlobalConstants::MP3_ADDRESS}/intro2.2.mp3"]
      },{
        action: 'input',
        eventUrl: ["#{GlobalConstants::V2_ADDRESS}/answer_action_selected"],
        timeout: TIMEOUT,
        maxDigits: 1
      }
    ]
  end

  def self.answer_action_selected(params, account)
    dtmf = params[:dtmf].to_i
    actions = []
    if dtmf == 1
      actions += [
        {
          action: 'stream',
          streamUrl: ["#{GlobalConstants::MP3_ADDRESS}/intro3.0.mp3"]
        },{
          action: 'talk',
          voiceName: 'Jennifer',
          level: 1,
          text: account.amount
        },{
          action: 'stream',
          streamUrl: ["#{GlobalConstants::MP3_ADDRESS}/intro3.1.mp3"]
        },{
          action: 'talk',
          voiceName: 'KJennifer',
          level: 1,
          text: account.account_no.gsub(/(?<=\d)(?=\d)/, ' ')
        },{
          action: 'stream',
          streamUrl: ["#{GlobalConstants::MP3_ADDRESS}/intro3.2.mp3"]
        }]
      actions += option_actions
    end
  end

  def self.payment_option_selected(params, account)
    dtmf = params[:dtmf].to_i
    actions = []
    option_mp3 = ""

    case dtmf
    when 1
      option_mp3 = "#{GlobalConstants::MP3_ADDRESS}/#{account.my_type.downcase}_option_one.mp3"
      send_sms(account, MessageHelper.sms_one)
    when 2
      option_mp3 = "#{GlobalConstants::MP3_ADDRESS}/#{account.my_type.downcase}_option_two.mp3"
      send_sms(account, MessageHelper.sms_two)
    when 3
      option_mp3 = "#{GlobalConstants::MP3_ADDRESS}/option_three.mp3"
      send_sms(account, MessageHelper.sms_three)
    when 4
      option_mp3 = "#{GlobalConstants::MP3_ADDRESS}/#{account.my_type.downcase}_option_four.mp3"
      send_sms(account, MessageHelper.sms_four)
    when 5
      option_mp3 = "#{GlobalConstants::MP3_ADDRESS}/#{account.my_type.downcase}_option_five.mp3"
      send_sms(account, MessageHelper.sms_five)
    when 6
      actions += [
        {
          action: 'talk',
          voiceName: 'Jennifer',
          text: 'Please wait while we connect you',
          level: 1
        },{
          action: 'connect',
          timeout: TIMEOUT_TRANSFER,
          eventUrl: ["#{GlobalConstants::V2_ADDRESS}/transfer/event"],
          from: account.decrypted_mobtel,
          endpoint: [
            {
              type: 'phone',
              number: GlobalConstants::TRANSFER_HOTLINE
            }
          ]
        }
      ]
    when 7
      actions += option_actions
    end

    if [1,2,3,4,5].include? dtmf
     actions += [
       {
         action: 'stream',
         streamUrl: [option_mp3]
       }]
     actions += repeat_actions(dtmf)
   end

   unless dtmf == 7
     if account.call_retry == 1
       account.fa_call_status = Account::ANSWERED
     elsif account.call_retry == 2
       account.sa_call_status = Account::ANSWERED
     end

     account.save
   end

        account.option_selected_logs.create(option: dtmf)
        actions
  end

  def self.repeat_info_selected(params, account)
    dtmf = params[:dtmf].to_i
    option = params[:option].to_i
    actions = []
    option_mp3 = ""

    if dtmf == 1
      my_type = account.my_type.downcase
      case option
      when 1
        option_mp3 = "#{GlobalConstants::MP3_ADDRESS}/#{account.my_type.downcase}_option_one.mp3"
      when 2
        option_mp3 = "#{GlobalConstants::MP3_ADDRESS}/#{account.my_type.downcase}_option_two.mp3"
      when 3
        option_mp3 = "#{GlobalConstants::MP3_ADDRESS}/#{account.my_type.downcase}_option_three.mp3"
      when 4
        option_mp3 = "#{GlobalConstants::MP3_ADDRESS}/#{account.my_type.downcase}_option_four.mp3"
      when 5
        option_mp3 = "#{GlobalConstants::MP3_ADDRESS}/#{account.my_type.downcase}_option_five.mp3"
      end

      actions += [
        {
          action: 'stream',
          streamUrl: [option_mp3]
        }]
      actions += repeat_actions(option)
    elsif dtmf == 9
      actions += option_actions
    end
    actions
  end

  def self.option_actions
    [
      {
        action: 'stream',
        streamUrl: ["#{GlobalConstants::MP3_ADDRESS}/options.mp3"],
        bargeIn: true
      },{
        action: 'input',
        eventUrl: ["#{GlobalConstants::V2_ADDRESS}/payment_option_selected"],
        timeout: TIMEOUT,
        maxDigits: 1
      }
    ]
  end

  def self.repeat_actions(dtmf = nil)
    [
      {
        action: 'stream',
        streamUrl: ["#{GlobalConstants::MP3_ADDRESS}/options_again.mp3"],
        bargeIn: true
      },{
        action: 'input',
        eventUrl: ["#{GlobalConstants::V2_ADDRESS}/repeat_info_selected?option=#{dtmf}"],
        timeout: TIMEOUT,
        maxDigits: 1
      }
    ]
  end

  def self.send_sms(account, msg)
    if account.decrypted_mobtel.length > 9
      sms = account.sms_messages.create!(message: msg, call_retry: account.call_retry, send_sms_attempt: 1)
      GlobelabsApiSmsWorker.perform_in((Time.now + 15.seconds),
                                       {
                                         message: msg
                                       },
                                       sms.id,
                                       account.id) if sms
    end
  end

  def self.hangup(params)
    account = Account.where(conversation_uuid: params[:conversation_uuid]).first

    if account.call_retry == 1
      account.fa_call_status = Account::ANSWERED
      send_sms(account, MessageHelper.message({amount:account.amount, account: account.account_no, fname: account.fname}))
    elsif account.call_retry == 2
      account.sa_call_status = Account::ANSWERED
    end

    if account.option_selected_logs.blank?
      account.option_selected_logs.create(option: '10')
    end
    account.save

    account.save
    Log.create(mobtel: account.mobtel,
               process_type: Log::PROCESS_TYPE_RESPONSE,
               request_type: Log::REQUEST_TYPE_CALL,
               status: Log::STATUS_SUCESS,
               params: "#{params.to_json}")
  end
end
