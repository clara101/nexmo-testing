class CallController < ApplicationController

  skip_before_action :verify_authenticity_token
  skip_before_action :authenticate_user!

  before_action :get_json_params, only: [:start_calling,
                                         :hangup,
                                         :incomplete_call,
                                         :ask_answered,
                                         :payment_options_answered,
                                         :incomplete_call_transfer,
                                         :incomplete_transfer,
                                         :hangup_transfer,
                                         :go_back_main_menu_answered]

  def index
    call_retry = params[:call_retry].to_i
    call_type = call_retry == 1 ? [nil, 0] : 1
    total = Account.where(call_retry: call_type, process_date: Date.today).count


    @unique_first = true
    @unique_second = true
    jobs = Sidekiq::ScheduledSet.new
    jobs.each do |job|
      if job.klass == 'FirstAttemptWorker' && call_retry.to_i == 1
        @unique_first = false
        break
      elsif job.klass == 'SecondAttemptWorker' && call_retry.to_i == 2
        @unique_second = false
        break
      end
    end

    if call_retry == 1 && @unique_first
      FirstAttemptWorker.perform_in((Date.today + 8.hours), get_default_size)
    elsif call_retry == 2 && @unique_second
      SecondAttemptWorker.perform_in((Date.today + 13.hours), get_default_size)
    end
    render json: { status: "Ok", accounts: total }
  end

  def start_calling
    session[:ctr] = 0
    render json: GlobelabsCallService.call(@json_params)
  end

  def ask
    render json: GlobelabsCallService
                   .ask( {
                           ctr: session[:ctr],
                           fname: params[:fname]
                         },
                         params[:acc] )
  end

  def ask_payment_options
    render json: GlobelabsCallService
                   .ask_payment_options( { ctr: session[:ctr] },
                                           params[:acc])
  end

  def ask_answered
    session[:ctr] += 1
    if session[:ctr] < 10
      render json: GlobelabsCallService.ask_answered(@json_params,
                                                     params[:acc])
    else
      GlobelabsCallService.hangup(@json_params, params[:acc])
      render nothing: true
    end
  end

  def payment_options_answered
    session[:ctr] += 1
    if session[:ctr] < 10
      render json: GlobelabsCallService
                    .payment_options_answered(@json_params, params[:acc])
    else
      GlobelabsCallService.hangup(@json_params, params[:acc])
      render nothing: true
    end
  end

  def incomplete_call_transfer
    GlobelabsCallService.incomplete_call_transfer(@json_params, params[:acc])
    render nothing: true
  end

  def incomplete_transfer
    GlobelabsCallService.incomplete_transfer(@json_params, params[:acc])
    render nothing: true
  end

  def hangup_transfer
    GlobelabsCallService.hangup_transfer(@json_params, params[:acc])
    render nothing: true
  end

  def hangup
    GlobelabsCallService.hangup(@json_params, params[:acc])
    render nothing: true
  end

  def incomplete_call
    GlobelabsCallService.validate_incomplete_call(@json_params, params[:acc])
    render nothing: true
  end

  def go_back_main_menu_answered
    render json: GlobelabsCallService
                   .go_back_main_menu_answered(@json_params,
                                               params[:option_selected],
                                               params[:acc])
  end

  def go_back_main_menu_hangup
    render json: {}
  end

  private

  def get_json_params
    @json_params = Tropo::Generator.parse request.env["rack.input"].read
  end

end
