class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :authenticate_user!
  def get_filename(type)
    type
  end

  def get_option(value)
    name = ""
    if value.to_i.eql?(10)
      name = "NoOption"
    elsif value.to_i.eql?(1)
      name = "Option1"
    elsif value.to_i.eql?(2)
      name = "Option2"
    elsif value.to_i.eql?(3)
      name = "Option3"
    elsif value.to_i.eql?(4)
      name = "Option4"
    elsif value.to_i.eql?(5)
      name = "Option5"
    elsif value.to_i.eql?(8)
      name = "SuccessTransfer"
    elsif value.to_i.eql?(9)
      name = "FailedTransfer"
    else
      name = ""
    end
  end

  def get_default_size
    100
  end

  def is_numeric?
    true if Float(self) rescue false
  end

  def get_err_msg(value)
    value = value ? value : ""
    number = value.eql?("") ? "Blank" : value
    if (['8','9','10','11','12'].include? value.size.to_s) && value.to_i.is_a?(Numeric) && value.to_i.to_s.eql?(value)
      msg = "Duplicate mobile number(#{number}) found in file. Please reupload."
    else
      msg = "Invalid mobile number(#{number}) found in file. Please reupload."
    end
  end

  def validate_format(mobtel,account,amount,name,type)
    if (mobtel =~ /^[a-zA-Z\d\s]*$/).nil?
      err = mobtel
    elsif (account =~ /^[\d]+(\.[\d]+){0,1}$/).nil?
      err = account
    elsif (amount =~ /^[\d]+(\.[\d]+){0,1}$/).nil?
      err = amount
    elsif (name =~ /^[a-zA-Z\d\s\.\,\-\u00F1]*$/).nil?
      err = name
    elsif !type.downcase.eql?("mobile") && !type.downcase.eql?("landline")
      err = type
    else
      err = "valid"
    end
    return err
  end
end
