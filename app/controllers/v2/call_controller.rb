class V2::CallController < ApplicationController
  skip_before_action :verify_authenticity_token
  skip_before_action :authenticate_user!
  before_action :set_timeout, only: [:event]
  before_action :set_account, except: [:start_calling, :event]

  def start_calling
    response = GlobelabsV2CallService.new.call(number_format(params[:mobile_number]))
    account = Account.find params[:account_id]
    call_retry = params[:call_retry].to_i
    attrs = {
      conversation_uuid: response['conversation_uuid'],
      uuid: response[':uuid'],
      call_retry: params['call_retry'],
    }
    additional_attrs = {}

    if call_retry == 1
      additional_attrs = {fa_call_attempt: 1, fa_call_status: Account::CALL_STATUS[:started]}
    elsif call_retry == 2
      additional_attrs = {sa_call_attempt: 1, sa_call_status: Account::CALL_STATUS[:started]}
    end

    account.update_attributes(attrs.merge(additional_attrs))

    Log.create(mobtel: account.mobtel,
               process_type: Log::PROCESS_TYPE_REQUEST,
               request_type: Log::REQUEST_TYPE_CALL,
               status: Log::STATUS_PROCESS,
               params: "#{params.to_json}")

    head :ok
  end

  def event
    account = Account.where(uuid: params[:uuid]).first_or_create
    attributes = {conversation_uuid: params[:conversation_uuid]}
    status = {}
    call_status = Account::CALL_STATUS[params[:status]]

    unless call_status == Account::CALL_STATUS[:completed]
      status = account.call_retry == 1 ? {fa_call_status: call_status} : {sa_call_status: call_status}
    end

    account.update_attributes(attributes.merge(status))
    head :ok
  end

  def transfer_event
   account = Account.where(conversation_uuid: params[:conversation_uuid]).first_or_create
   if params[:status] == 'answered'
     account.option_selected_logs.create(option: '7')
   else
     account.option_selected_logs.create(option: '6')
   end
   head :ok
  end

  def answer
    @action = GlobelabsV2CallService.answer(@account)
    render_json
  end

  def answer_action_selected
    @action = GlobelabsV2CallService.answer_action_selected(params, @account)
    render_json
  end

  def payment_option_selected
    @action = GlobelabsV2CallService.payment_option_selected(params, @account)
    render_json
  end

  def repeat_info_selected
    @action = GlobelabsV2CallService.repeat_info_selected(params, @account)
    render_json
  end

  private
  def set_account
    @account = Account.where(conversation_uuid: params[:conversation_uuid]).first
  end

  def set_timeout
    GlobelabsV2CallService.hangup(params) if Account::CALL_STATUS[params[:status]] == Account::CALL_STATUS[:completed]
  end

  def render_json
    render json: @action
  end

  def number_format(number)
    "63#{number.last(10)}"
  end

end
