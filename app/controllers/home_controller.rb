require 'csv'
require 'message_helper'

class HomeController < ApplicationController
  before_action :authenticate_user!, :except => [:receive]
  skip_before_action :verify_authenticity_token

  def index
  end

  def release
  end

  def receive
    msg = params['inboundSMSMessageList']['inboundSMSMessage'][0]['message'].to_s.downcase
    msisdn = params['inboundSMSMessageList']['inboundSMSMessage'][0]['senderAddress'].gsub('tel:+63','')
    if msg.split(' ').count.eql?(3)
      response = HTTParty.post("https://devapi.globelabs.com.ph/smsmessaging/v1/outbound/#{GlobalConstants::SHORT_CODE}/requests?app_id=#{GlobalConstants::APP_ID}&app_secret=#{GlobalConstants::APP_SECRET}", :body => {passphrase: GlobalConstants::PASSPHRASE,message: MessageHelper.correct_format, address: msisdn})
      Message.create!(mobtel:msisdn, message:msg)
    else
      response = HTTParty.post("https://devapi.globelabs.com.ph/smsmessaging/v1/outbound/#{GlobalConstants::SHORT_CODE}/requests?app_id=#{GlobalConstants::APP_ID}&app_secret=#{GlobalConstants::APP_SECRET}", :body => {passphrase: GlobalConstants::PASSPHRASE,message: MessageHelper.wrong_format, address: msisdn})
    end
  end

  def upload
    err = "valid"
    last_mobtel, last_type = Account.upload_accounts(params)
    # if last_mobtel.eql? "wf"
    #   response = "Invalid value (#{err}) found in file. Remove special characters and reupload."
    # else
      err = ( !last_type.downcase.eql?("landline") && !last_type.downcase.eql?("mobile") ) ? "Invalid account type (#{last_type}). Choose only from Mobile or Landline" : get_err_msg(last_mobtel)
      response = !last_mobtel.eql?("none") ? err : "File has been successfully uploaded."
    # end

    render json: { response: response }
  end

  def reset_accounts
    call_retry = params[:call_retry].to_i
    accounts = Account.for_today
    if accounts
      accounts.each do |account|
        account.sms_messages.destroy_all
        account.option_selected_logs.destroy_all
        if call_retry == 1
          account.update_attributes(call_retry: 0, fa_call_attempt: nil, fa_call_status: nil, sa_call_attempt: nil, sa_call_status: nil)
        else
          account.update_attributes(call_retry: 1, sa_call_attempt: nil, sa_call_status: nil)
        end
      end
      render plain: "#{accounts.count} account/s reset"
    else
      render plain: "No accounts found for today"
    end
  end

  def start_sidekiq
    sidekiq_log = system "locate sidekiq.log -n 1"
    system "cd #{Rails.root} & bundle exec sidekiq --environment production -d -L #{sidekiq_log}"
    render nothing: true
  end
end
