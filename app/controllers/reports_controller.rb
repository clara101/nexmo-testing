require 'csv'

class ReportsController < ApplicationController
  respond_to :html, :json
  skip_before_action :verify_authenticity_token, only: [:delete]
  before_filter :get_date_params, only: [:filter_by_params,
                                         :option_selected,
                                         :sms_sent_data,
                                         :sms_received_data,
                                         :summary_reports]

  def index
  end

  def selected_options
  end

  def summary_reports
    session[:tab] = 'report'
    report_type = params[:type]
    opts = {
      date_from: @date_from,
      date_to: @date_to,
      report_type: report_type,
      selected_month: params[:selected_month]
    }

    reports = Account.summary_reports_call(opts)
    respond_with(ReportJsonSerializer.summary_reports_to_json(reports.page(params[:page]), report_type))
  end

  def filter_by_params
    # search reports from date to date
    reports = Account.summary(@date_from..@date_to).page params[:page]

    # generate report response structure and response it into json
    respond_with(ReportJsonSerializer.summary_to_json(reports))
  end

  def option_selected
    date_from = Date.parse(params["date_from"]).beginning_of_day
    date_to = Date.parse(params["date_to"]).end_of_day

    reports = OptionSelectedLog.summary(date_from..date_to).page params[:page]

    # generate report response structure and response it into json
    respond_with(ReportJsonSerializer.option_selected_to_json(reports))
  end

  def sms_sent
  end

  def sms_sent_data
    date_from = Date.parse(params["date_from"]).beginning_of_day
    date_to = Date.parse(params["date_to"]).end_of_day

    reports =
      SmsMessage
      .select("date(created_at) AS process_date, COALESCE(sum(CASE WHEN is_sent THEN 1 ELSE 0 END),0) AS sms_sent_total")
      .where(created_at: date_from..date_to)
      .group("date(created_at)")
      .order("date(created_at) DESC")
      .page params[:page]

    respond_with(ReportJsonSerializer.sms_sent_to_json(reports))
  end

  def sms_received
  end

  def sms_received_data
    reports = Message.select("date(created_at) AS process_date, COUNT(messages.mobtel) AS sms_total").where(created_at: @date_from.beginning_of_day..@date_to.end_of_day).group("date(created_at)").order("date(created_at) DESC").page params[:page]
    respond_with(ReportJsonSerializer.sms_received_to_json(reports))
  end

  def download_messages
    date = Date.parse(params[:date])
    m = Message.where(created_at: date.to_time.all_day)
    builder = CSV.generate do |csv|
        m.each do |item|
          msg = item.message.downcase.gsub('yes ','').split(' ', 2)
          csv << [item.mobtel,"no.#{msg[0]}",msg[1],item.created_at.strftime("%Y-%m-%d")]
        end
      end
    send_data builder, :type => 'text/csv; charset=utf-8;', :disposition => "attachment;filename=messages-#{date.strftime("%Y-%m-%d")}.csv"
  end

  def filter_by_option_selected
    date = Date.parse(params[:date])
    option = params[:option]
    filename = get_option(option)
    accounts =
      Account
      .select(:mobtel, :updated_at)
      .joins(:option_selected_logs)
      .where('date(option_selected_logs.created_at)=? AND option_selected_logs.option=?', date, option)
    send_data accounts.to_csv, filename: "#{filename}-#{params[:date]}.csv"
  end

  def generate_csv
    date = Date.parse(params[:date])
    call_status = params[:call_status].to_i
    call_retry = !params[:call_retry].nil? ? params[:call_retry].to_i : 0

    accounts =
      if call_retry == 1
        Account
        .select(Account::ATTR_DOWNLOADABLES)
        .where(process_date:date, fa_call_status: call_status)
      elsif call_retry == 2
        Account
        .select(Account::ATTR_DOWNLOADABLES)
        .where(process_date:date, sa_call_status: call_status)
      else
        Account
        .select(Account::ATTR_DOWNLOADABLES)
        .where("process_date = ? and (fa_call_status = ? or sa_call_status = ?)", date, call_status, call_status)
      end

    file = get_filename({call_retry: call_retry.to_i, call_status:call_status.to_i})
    send_data accounts.to_csv, filename: "#{file}-#{params[:date]}.csv"
      # send_data answered.to_csv
  end

  def delete
    accounts = Account.where(process_date: params[:process_date])
    response =
      if accounts
        Account.delete_all(process_date: params[:process_date])
        "Success"
      else
        "No data on date."
      end

    render json: { response: response }
  end

  def generate_report
      report_type = params[:dl_type]
      start_date = Date.parse(params[:dl_start_date])
      end_date = Date.parse(params[:dl_end_date])
      filename = get_filename(report_type)

      reports = Account.report_csv(start_date, end_date, report_type)
      today = Date.today
      send_data(reports.to_report_download, filename: "#{filename}_#{today}_#{report_type}.csv")
    end

  private

  def get_date_params
    @date_from = (params[:start_date] ? Date.parse(params[:start_date]) : Date.today).beginning_of_day
    @date_to = (params[:end_date] ? Date.parse(params[:end_date]) : Date.today).end_of_day
  end
end
