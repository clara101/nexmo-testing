class SecondAttemptWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(size)
    return if Time.now.hour > 19
    next_time = Time.now

    ss = Sidekiq::ScheduledSet.new
    jobs = ss.select {|retri| retri.klass == "GlobelabsApiCallWorker" }.each(&:delete)

    Account.where(process_date: Time.now.all_day, fa_call_status: [nil,1,0]).each do |item|
      item.fa_call_status = 3
      item.save!
    end

    if !Account.where(process_date: Date.today, fa_call_status: [Account::REJECTED_AND_BUSY, Account::MISSED_CALL, Account::UNREACHABLE], sa_call_status: [0,nil]).empty?
      Account.select("id, mobtel").where(process_date: Date.today, fa_call_status: [Account::REJECTED_AND_BUSY, Account::MISSED_CALL, Account::UNREACHABLE], sa_call_status: [0,nil]).find_in_batches(batch_size: size) do |batch|
        batch.each do |account|
          GlobelabsApiCallWorker.perform_in(next_time, 2, account.id)
        end
        next_time += 1.minute
      end
    end
  end
end
