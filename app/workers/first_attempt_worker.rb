class FirstAttemptWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(size)
    next_time = Time.now
    time_hour = next_time.hour
    #set up timezone
    if time_hour > 7 && time_hour < 13
      #empty current queue list
      ss = Sidekiq::ScheduledSet.new
      ss.clear
      #blast again with new uploaded account if any
      Account.select("id, mobtel, fa_call_status").where(process_date: Date.today, fa_call_status: [0,1,nil]).find_in_batches(batch_size: size) do |batch|
        batch.each do |account|
          GlobelabsApiCallWorker.perform_in(next_time, 1, account.id)
        end
        next_time += 1.minute
      end
    end
  end
end
