require 'globe_numbers'

class GlobelabsApiCallWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(call_retry, account_id)
    account = Account.find(account_id)
    mobile_number = account.decrypted_mobtel
    voice_app_token =
      if globe(mobile_number)
        GlobalConstants::VOICE_APP_TOKEN
      else
        GlobalConstants::VOICE_APP_TOKEN_2
      end

    time_hour = Time.now.hour
    if time_hour > 7 && time_hour < 19
      Log.create(mobtel: mobile_number,
                 process_type: Log::PROCESS_TYPE_REQUEST,
                 request_type: Log::REQUEST_TYPE_CALL,
                 status: Log::STATUS_PROCESS,
                 params: "#{ENV['TROPO_URL']}/sessions?action=create&token=#{voice_app_token}&mobile_number=#{mobile_number}&call_retry=#{call_retry}&account_id=#{account_id}")

      request = Typhoeus::Request.new("#{GlobalConstants::V2_ADDRESS}/start_call",
                             method: :post,
                             body: {
                               mobile_number: mobile_number,
                               call_retry: call_retry,
                               account_id: account_id
                             }
                            )
      request.on_complete do |response|
        unless response.code == 200
          puts "###################------#{response.body} -------#{response.code}"

          Log.create(mobtel: mobile_number,
                     process_type: Log::PROCESS_TYPE_RESPONSE,
                     request_type: Log::REQUEST_TYPE_CALL,
                     status: Log::STATUS_FAILED,
                     params: response.body)
        else
          Log.create(mobtel: mobile_number,
                     process_type: Log::PROCESS_TYPE_RESPONSE,
                     request_type: Log::REQUEST_TYPE_CALL,
                     status: Log::STATUS_PROCESS_SUCCESS,
                     params: response.body)
        end
      end
      request.run
    end
  end

  def globe(mobile_number)
    GlobeNumbers.globe?(mobile_number[0..3]) ||
    GlobeNumbers.globe?(mobile_number[0..4])
  end
end
