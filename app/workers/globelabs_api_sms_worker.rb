require 'globe_numbers'

class GlobelabsApiSmsWorker
  include Sidekiq::Worker

  def perform(json, sms_id, account_id)
    sms = SmsMessage.find(sms_id)
    account = Account.find(account_id)

    app_id = GlobalConstants::APP_ID
    app_secret = GlobalConstants::APP_SECRET
    short_code = GlobalConstants::SHORT_CODE

    app_id_2 = GlobalConstants::APP_ID_2
    app_secret_2 = GlobalConstants::APP_SECRET_2
    short_code_2 = GlobalConstants::SHORT_CODE_2

    mobtel = account.decrypted_mobtel

    if json['address'].blank?
      json.merge!('address' => mobtel,
                  'deliveryNotification' => 'true')
    end

    Log.create(mobtel: account.mobtel,
               process_type: Log::PROCESS_TYPE_REQUEST,
               request_type: Log::REQUEST_TYPE_SMS,
               status: Log::STATUS_PROCESS,
               params: "#{json.to_json}")

    response =
      if GlobeNumbers.globe?(mobtel[0..3]) || GlobeNumbers.globe?(mobtel[0..4])
        json.merge!(passphrase: GlobalConstants::PASSPHRASE) if json['passphrase'].blank?
        HTTParty.post("#{ENV['SMS_URL']}/#{short_code}/requests?app_id=#{app_id}&app_secret=#{app_secret}", body: json)
      else
        json.merge!(passphrase: GlobalConstants::PASSPHRASE_2) if json['passphrase'].blank?
        HTTParty.post("#{ENV['SMS_URL']}/#{short_code_2}/requests?app_id=#{app_id_2}&app_secret=#{app_secret_2}", body: json)
      end

    if response.code == 201
      sms.update_attribute(:is_sent, true)
      Log.create(mobtel: account.mobtel,
                 process_type: Log::PROCESS_TYPE_RESPONSE,
                 request_type: Log::REQUEST_TYPE_SMS,
                 status: Log::STATUS_PROCESS_SUCCESS,
                 params: response.body)
    else
      sms.update_attributes(send_sms_attempt: (sms.send_sms_attempt + 1)) if sms.send_sms_attempt < 20
      Log.create(mobtel: account.mobtel,
                 process_type: Log::PROCESS_TYPE_RESPONSE,
                 request_type: Log::REQUEST_TYPE_SMS,
                 status: Log::STATUS_FAILED,
                 params: response.body)

      GlobelabsApiSmsWorker.perform_in(GlobalConstants::RETRY_AFTER, json, sms_id, account_id)

    end
  end
end
