$(function() {
  $("a[data-toggle='tab']").on('click', function(e){
    $("li.selected").removeClass('selected');
    $(this).parent().addClass('selected');

    angular
    .element(document.getElementById('rpt-ctrl'))
    .scope()
    .getSummaryReport();
  });

  var curHour = new Date().getHours();
  var curDay = new Date().getDay();
  var start = (curHour > 13) ? '+1d' : '-1d';
  if(curDay == 1){
    start = (curHour > 21) ? '+1d' : '-1d';
  }

  $('.input-group.upload').datepicker({
    autoclose: true,
    todayHighlight: true,
    format: 'yyyy-mm-dd',
    startDate: start,
    daysOfWeekDisabled: [0]
  });

  $('.input-group.date').datepicker({
    autoclose: true,
    todayHighlight: true,
    format: 'yyyy-mm-dd'
  });

  $("#downloadLink, #generate_button").on('click', function(e){
  var startDate = $('#start-date').val();
  var endDate = $('#end-date').val();
  var type = $("li.selected a")[0].text.toLowerCase();

  $('input[name=dl_type]').val(type);
  $('input[name=dl_start_date]').val(startDate);
  $('input[name=dl_end_date]').val(endDate);
});
});
