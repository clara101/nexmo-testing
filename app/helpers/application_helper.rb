module ApplicationHelper
  def encrypt_account(id)
    Encryptor.call_encrypt(id)
  end

  def decrypt_account(id)
    Encryptor.call_decrypt(id)
  end

  def find_account(acc)
    # acc_id = Encryptor.call_decrypt(acc).to_i
    # Account.find(acc_id)
    Account.find(acc)
  end
end
