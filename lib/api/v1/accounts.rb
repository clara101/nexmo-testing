module API
  module V1
    class Accounts < Grape::API
      version 'v1'
      format :json

      params do
        optional :date, type: String
      end

      resource :accounts do
        desc 'get accounts'
        get do
          query = { process_date: Date.today }
          if params[:date].present?
            date = Date.parse(params[:date])
            query = { process_date: date }
          end
          accounts = Account.where(query)
          size = accounts.size
          first_processed = accounts.where(fa_call_status: Account::PROCESSED).size
          first_unprocessed = accounts.where(fa_call_status: [nil, 1, 0]).size
          second_processed = accounts.where(call_retry: 2, sa_call_status: Account::PROCESSED).size
          second_unprocessed = accounts.where(call_retry: 2, sa_call_status: [nil, 1, 0]).size
          {
            size: size,
            first_processed: first_processed,
            first_unprocessed: first_unprocessed,
            second_processed: second_processed,
            second_unprocessed: second_unprocessed
          }
        end
      end
    end
  end
end
