module MessageHelper

  def self.intro
    "Hi! This call is for"
  end
  def self.intro2(value)
    "This is a friendly reminder from Globe Home Broadband and/or Globe Landline. You may have overlooked payment of the overdue balance amounting to #{value[:amount]} for broadband and/or landline account number #{value[:account]}. Remember to keep your payments updated so you can enjoy your broadband and/or landline service without interruption. You can settle your balance in several convenient ways."
  end

  def self.options(value)
    "#{GlobalConstants::APP_ADDRESS}intro2.0.mp3 #{value[:fname]},  #{GlobalConstants::APP_ADDRESS}intro2.1.mp3 #{GlobalConstants::APP_ADDRESS}intro2.2.mp3 #{value[:fname]} #{GlobalConstants::APP_ADDRESS}intro2.3.mp3"
  end

  def self.options2
    "To pay online, press 1. For over-the-counter payment centers, press 2. To enroll in Auto Pay, press 3. For online banking, press 4. To pay using GCash, press 5. To speak to our customer service representative, press 6. To hear the options again, press 7."
  end

  def self.options3
    "To repeat the given info, press 1. To go back to the main menu, press 9."
  end

  #=====VOICE=====#
  def self.mobile_one
    "Pay online using credit card or GCash at www.globe.com.ph/paybill. We'll send you this information shortly via text. Thank you!"
  end
  def self.mobile_two
    "We'll send you the list of our accredited over-the-counter payment partners shortly via text. Thank you!"
  end
  def self.mobile_three
    "Experience the convenience of having your monthly bills automatically charged to your credit card, assuring you'll never miss another bill payment. To enroll in Auto Pay, please contact your credit card provider. Thank you!"
  end
  def self.mobile_four
    "We'll send you the list of our accredited bank partners shortly via text. Thank you!"
  end
  def self.mobile_five
    "Dial *143# from your Globe mobile, select GCASH then Pay Bills. After providing the required information, wait for the confirmation message. We'll send you this information shortly via text. Thank you!"
  end

  def self.landline_one
    "Pay online using credit card or GCash at www.globe.com.ph/paybill. Thank you!"
  end
  def self.landline_two
    "Pay your Globe bills through any of our accredited over-the-counter payment partners near you! BDO, BPI, Chinatrust, East West Bank, Planters Bank, PNB, Security Savings Bank, RCBC, Robinsons Bank, Security Bank, Union Bank, One Network Bank, SM Mart, Bayad Center, E C Pay, M Lhuillier, Cebuana Lhuillier. Thank you!"
  end
  def self.landline_three
    "Experience the convenience having your monthly bills automatically charged to your credit card, assuring you'll never miss another bill payment. To enroll in Auto Pay, please contact your credit card provider. Thank you!!"
  end
  def self.landline_four
    "Pay your Globe Home Broadband or Globe Landline bills online thru our Bank partners: 1. BPI (Express On-Line) at www.bpiexpressonline.com. 2. BDO at www.bdo.com.ph . 3. MBTC (Metro Direct) at https://personal.metrobankdirect.com. 4. Bancnet On-Line at  www.bancnetonline.com. 5. Union Bank at www.unionbankph.com. Thank you!"
  end
  def self.landline_five
    "Dial * 1 4 3 # from your Globe mobile, select GCASH then Pay Bills. After providing the required information, wait for the confirmation message. Thank you!"
  end

  def nega_one
    "We're sorry, but our online payment service is currently unavailable. To hear our other convenient payment channels, please press 9. Thank you!"
  end
  def nega_two
    "We're sorry, but over-the-counter payments are currently unavailable. To hear our other convenient payment channels, please press 9. Thank you!"
  end
  def nega_three
    "We're sorry, but we're unable to process Auto Pay enrollments. Please contact your credit card provider to enroll in this service or press 9 to hear our other convenient payment channels. Thank you!"
  end
  def nega_four
    "We're sorry, but payment via online banking is currently unavailable. To hear our other convenient payment channels, please press 9. Thank you!"
  end
  def nega_five
    "We're sorry,but payment via GCash is currently unavailable. To hear our other convenient payment channels, please press 9. Thank you!"
  end
  #=====VOICE=====#

  #=====SMS=====#
  def self.temp_message(answer, type)
    "Hi, this is the corresponding message for. option #{answer} for the #{type} type account."
  end
  def self.message(value)
    "Hi! We were calling to remind you of the overdue balance of #{value[:amount]} for Globe Home Broadband and/or Landline account number #{value[:account]}. To see our convenient payment channels, visit www.globe.com.ph/help/postpaid/billing. Data charges may apply. Please disregard this message if payment has been made. Thank you!"
  end

  def self.sms_one
    "Enjoy hassle-free bill payment when you go online. Settle your Globe bills using credit card or GCash. Receive your official receipt through your email. Try Online Bill Payment at www.globe.com.ph/paybill. Data charges may apply. Thank you!"
  end
  def self.sms_two
    "Pay your Globe bills through any of our accredited over-the-counter payment partners near you!\nAllied Bank\nBDO\nBPI\nChinatrust\nEastWest Bank\nPlanters Bank\nPNB\nSecurity Savings Bank\nRCBC\nRobinsons Bank\nSecurity Bank\nUnion Bank\nLand Bank (iAccess)\nOne Network Bank\nLuzon Development Bank\nSM Mart\nBayad Center\nECPay\nM Lhuillier\nCebuana Lhuillier"
  end
  def self.sms_three
    "Experience the convenience of having your monthly bills automatically charged to your credit card, assuring you'll never miss another bill payment. To enroll in Auto Pay, please contact your credit card company. Thank you!"
  end
  def self.sms_four
    "Pay your Globe bills online thru our bank partners:\n1. BPI (Express Online) www.bpiexpressonline.com\n2. BDO www.bdo.com.ph\n3. MBTC (MetroDirect) www.personal.metrobankdirect.com\n4. Bancnet Online  www.bancnetonline.com\n5. Union Bank ebanking.unionbankph.com"
  end
  def self.sms_five
    "Pay your Globe bills using GCash.  Simply dial *143# on your Globe mobile, select GCash, then Pay Bills. You'll receive a text confirmation once your payment is successful. Not yet enrolled in GCash? Registration is easy. Dial *143#, select GCash, then choose Register to get started. Thank you!"
  end

  def self.sms_nega_one
    "We're sorry, but our online payment service is currently unavailable."
  end
  def self.sms_nega_two
    "We're sorry, but over-the-counter payments are currently unavailable."
  end
  def self.sms_nega_three
    "We're sorry, but we're unable to process Auto Pay enrollments."
  end
  def self.sms_nega_four
    "We're sorry, but payment via online banking is currently unavailable."
  end
  def self.sms_nega_five
    "We're sorry, but payment via GCash is currently unavailable."
  end
  #=====SMS=====#

end
