namespace :lipat do
  desc 'export legacy data'
  task account: :environment do
    File.write("db/seeds/accounts.rb", "")
    date_from = ENV['date_from']
    date_to = ENV['date_to']
    accounts = Account.where(process_date: (date_from..date_to))
    # accounts = Account.all
    return "No Accounts Found" if accounts.blank?
    accounts.each do |acc|
      puts "Account.create(mobtel: '#{acc.mobtel.gsub(/\n/, "\\n")}', account_no: '#{acc.account_no.to_s}', amount: '#{acc.amount.to_s}', fa_call_status: #{acc.fa_call_status}, fa_call_attempt: #{acc.fa_call_attempt || 0}, sa_call_status: #{acc.sa_call_status || 'nil'}, sa_call_attempt: #{acc.sa_call_attempt || 'nil'}, created_at: '#{acc.created_at.to_s}', updated_at: '#{acc.updated_at.to_s}', call_retry: #{acc.call_retry}, process_date: '#{acc.process_date.to_s}', fname: '#{acc.fname.gsub(/'/,'\\\\\'')}', my_type: '#{acc.my_type}', account_id: #{acc.id})"
    end
  end

  desc 'export sms data'
  task sms: :environment do
    File.write("db/seeds/sms.rb", "")
    date_from = ENV['date_from']
    date_to = ENV['date_to']
    accounts = Account.where(process_date: (date_from..date_to))
    return "No Accounts Found" if accounts.blank?
    accounts.each do |acc|
      sms_messages = acc.sms_messages
      next if sms_messages.blank?
      sms_messages.each do |sms|
        puts "acc = Account.find_by_account_id(#{sms.account_id})"
        puts "params = { message: '#{sms.message.gsub(/'/,'\\\\\'')}', is_sent: #{sms.is_sent || 'nil'}, account_id: acc.id, created_at: '#{sms.created_at}', updated_at: '#{sms.updated_at}', call_retry: #{sms.call_retry || 'nil'}, send_sms_attempt: #{sms.send_sms_attempt || 0} }"
        puts "SmsMessage.create(params)"
        #<SmsMessage id: 1, message: "Enjoy hassle-free bill payment when you go online....", is_sent: true, account_id: 29491, created_at: "2016-10-04 00:54:26", updated_at: "2016-10-04 00:54:49", call_retry: nil, send_sms_attempt: 1>
      end
    end
  end

  desc 'export option selected logs data'
  task option_log: :environment do
    File.write("db/seeds/option_logs.rb", "")
    date_from = ENV['date_from']
    date_to = ENV['date_to']
    accounts = Account.where(process_date: (date_from..date_to))
    return "No Accounts Found" if accounts.blank?
    accounts.each do |acc|
      option_logs = acc.option_selected_logs
      next if option_logs.blank?
      option_logs.each do |option_log|
        puts "acc = Account.find_by_account_id(#{option_log.account_id})"
        puts "if acc.present?"
        puts "params = { option: '#{option_log.option}', account_id: acc.id, created_at: '#{option_log.created_at}', updated_at: '#{option_log.updated_at}' }"
        puts "OptionSelectedLog.create(params)"
        puts "end"
        #<OptionSelectedLog id: 1, option: "10", account_id: 1, created_at: "2016-10-04 00:00:26", updated_at: "2016-10-04 00:00:26">
      end
    end
  end
end
