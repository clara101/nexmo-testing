namespace :sidekiq do
  desc 'start sidekiq'
  task start: :environment do
    sidekiq_log = %x[locate sidekiq.log -n 1].delete!("\n")
    system "cd #{Rails.root} & bundle exec sidekiq --environment production -d -L #{sidekiq_log}"
  end

  desc 'terminate sidekiq'
  task terminate: :environment do
    ss = Sidekiq::ScheduledSet.new
    jobs = ss.select{ |retri| retri.klass == "GlobelabsApiCallWorker" }
    jobs.each(&:delete)

    system "ps -ef | grep sidekiq | grep -v grep | awk '{print $2}' | xargs kill -9"
  end

  desc 'check if sidekiq is running'
  task status: :environment do
    time_hour = Time.now.hour
    puts time_hour > 7 && time_hour < 19
    if time_hour > 7 && time_hour < 19
      running = system "ps aux | grep '[s]idekiq' | grep vbb"
      unless running
        system "cd #{Rails.root}/ & bundle exec sidekiq -i 1 --environment production -d -L #{Rails.root}/../sidekiq.log"
      end
    end
  end
end
