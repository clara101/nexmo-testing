require 'httparty'
namespace :blast do
  desc 'blast first attempt'
  task first_attempt: :environment do
    numbers = ["9173141859"]
    url = "https://devapi.globelabs.com.ph/smsmessaging/v1/outbound/9897/requests?app_id=raMkf7E74jC7ocyobKT7xyCd5akKfKBG&app_secret=5bf63106bc69afaa395e90321b0aded736b391d67f11fdc81b57699c840a000c"
    numbers.each do |num|
      json = { passphrase: "lJ5XfMGGvU", message: "[Vbb] Blast #{Time.now}", address: num}
      response_SMS = HTTParty.post(url, body: json)
    end
    response = HTTParty.get("#{GlobalConstants::APP_ADDRESS}globelabs/call?call_retry=1")
    puts "the response is #{response.code}"
  end

  desc "blast second attempt"
  task second_attempt: :environment do
    numbers = ["9173141859"]
    url = "https://devapi.globelabs.com.ph/smsmessaging/v1/outbound/9897/requests?app_id=raMkf7E74jC7ocyobKT7xyCd5akKfKBG&app_secret=5bf63106bc69afaa395e90321b0aded736b391d67f11fdc81b57699c840a000c"
    numbers.each do |num|
      json = { passphrase: "lJ5XfMGGvU", message: "[Vbb] Second Attempt #{Time.now}", address: num}
      response_SMS = HTTParty.post(url, body: json)
    end

    response = HTTParty.get("#{GlobalConstants::APP_ADDRESS}globelabs/call?call_retry=2")
    puts "the response is #{response.code}"
  end

  desc "unprocessed to unreachable"
  task clean_up: :environment do
    fattempt = Account.where(process_date: Time.now.all_day, fa_call_status: [nil,1,0])
    if !fattempt.empty?
      fattempt.each do |a|
        a.fa_call_status = a.fa_call_status.eql?(1) ? 4 : 3
        a.save!
      end
    end
    sattempt = Account.where(process_date: Time.now.all_day, fa_call_status: [3,2,5], sa_call_status: [nil,1,0])
    if !sattempt.empty?
      sattempt.each do |a|
        a.sa_call_status = a.sa_call_status.eql?(1) ? 4 : 3
        a.save!
      end
    end
  end
end
