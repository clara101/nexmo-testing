class Encryptor
  class << self
    def encrypt(mobtel)
      AESCrypt.encrypt(mobtel, ENV['SECRET_TOKEN'])
    end

    def decrypt(mobtel)
      AESCrypt.decrypt(mobtel, ENV['SECRET_TOKEN'])
    end

    def call_encrypt(id)
      id.to_i * 8383
    end

    def call_decrypt(id)
      id.to_i / 8383
    end
  end
end
