'use strict';

/* Controllers */

var rptApp = angular.module('reports', ['ui.bootstrap']);

rptApp.controller('RptCtrl', function($scope, $http, $filter) {

  $scope.loading = false ;
  $scope.reports = [];
  $scope.startDate = new Date();
  $scope.endDate = new Date();
  $scope.selectedMonth = new Date();

  if(!!window.chrome){
    $scope.filterDateFrom = $filter('date')(new Date(),'yyyy-MM-dd');
    $scope.filterDateTo = $filter('date')(new Date(),'yyyy-MM-dd');
  }else{
    $scope.filterDateFrom = new Date();
    $scope.filterDateTo = new Date();
  }

  $scope.totalItems = 0;
  $scope.currentPage = 1;
  $scope.maxSize = 5;
  $scope.itemPerPage = 5;

  $scope.now = new Date(),'yyyy-MM-dd';
  $scope.hour = new Date().getHours();

  $scope.pageChanged = function() {
    $scope.getReportsByDate();
  };

  $scope.smsPageChanged = function() {
    $scope.getSmsSentReportsByDate();
  };

  $scope.optionSelectedPageChanged = function() {
    $scope.getSelectedOptionReportsByDate();
  };

  $scope.currentDate = new Date();

  $scope.getSummaryReport = function() {
    $scope.reportsBy = $("li.selected a[data-toggle='tab']").attr('reportBy');

    if ($("#start-date").val().length != 0){
      $scope.startDate = new Date($("#start-date").val());
    }

    if ($("#end-date").val().length != 0){
      $scope.endDate = new Date($("#end-date").val());
    }

    if ($("#start-week").val().length != 0){
      var s_year = $("#start-week").val().substring(0,4);
      var s_week = $("#start-week").val().substring(6,8);
      $scope.startWeek = $scope.getDateFromWeekNumber(s_year, s_week);
    } else {
      $scope.startWeek = new Date();
    }

    if ($("#end-week").val().length != 0){
      var e_year = $("#end-week").val().substring(0,4);
      var e_week = $("#end-week").val().substring(6,8);
      $scope.endWeek = $scope.getDateFromWeekNumber(e_year, e_week);
    } else {
      $scope.endWeek = new Date();
    }

    if ($("#selected-month").val().length != 0){
      $scope.selectedMonth = new Date($("#selected-month").val());
    }

    console.log("REporting: " + $scope.startDate );

    $scope.showLink = false;
    $scope.showSelectMonth = false;
    $scope.showSelectDate = false;

    console.log("REPORT BY: "+ $scope.reportsBy);

    if ($scope.reportsBy == 'daily'){
      // WEEKLY
      $http.get("/reports/summary_reports.json?type=daily&start_date="+$scope.startDate+"&end_date="+$scope.endDate+"&page="+$scope.currentPage)
      .success(
        function(response){
          $scope.daily_reports = response.data;
          console.log("DAILY QUERY");
          if (response.data.length == 0){
            $scope.totalItems = 0;
            $('#loader-container').show();
            $('.loader-data').html('<p class="text-info">No reports found.</p>');
            $scope.showLink = false;
            $scope.showSelectWeek = false;
            $scope.showSelectMonth = false;
            $scope.showSelectDate = true;
          } else {
            $('.loader-data').html('');
            $("#downloadLink").parent().attr('action', '/reports/generate_report?type=daily')
            $scope.showLink = true;
            $scope.showSelectWeek = false;
            $scope.showSelectMonth = false;
            $scope.showSelectDate = true;
            $scope.totalItems = response.pagination.total_items;
            $scope.itemPerPage = response.pagination.per_page;
            $('#loader-container').hide();
          }
        }
      );
    } else if ($scope.reportsBy == 'weekly') {
      $http.get("/reports/summary_reports.json?type=weekly&start_date="+$scope.startWeek+"&end_date="+$scope.endWeek+"&page="+$scope.currentPage)
      .success(
        function(response){
          $scope.weekly_reports = response.data;
          console.log("WEEKLY QUERY");
          if (response.data.length == 0){
            $scope.totalItems = 0;
            $("#loader-container.weekly").show();
            $('.loader-data').html('<p class="text-info">No reports found.</p>');
            $scope.showLink = false;
            $scope.showSelectWeek = true;
            $scope.showSelectMonth = false;
            $scope.showSelectDate = false;
          } else {
            $('input[name=dl_start_date]').val($scope.startWeek);
            $('input[name=dl_end_date]').val($scope.endWeek);
            $("#loader-container.weekly").hide();
            $('.loader-data').html('');
            $("#downloadLink").parent().attr('action', '/reports/generate_report?type=weekly')
            $scope.showLink = true;
            $scope.showSelectWeek = true;
            $scope.showSelectMonth = false;
            $scope.showSelectDate = false;
            $scope.totalItems = response.pagination.total_items;
            $scope.itemPerPage = response.pagination.per_page;
          }
        }
      );
    } else if ($scope.reportsBy == 'monthly') {
      $http.get("/reports/summary_reports.json?type=monthly&selected_month="+$scope.selectedMonth+"&page="+$scope.currentPage)
      .success(
        function(response){
          $scope.monthly_reports = response.data;
          console.log("MONTHLY QUERY");
          if (response.data.length == 0){
            $scope.totalItems = 0;
            $("#loader-container.monthly").show();
            $('.loader-data').html('<p class="text-info">No reports found.</p>');
            $scope.showLink = false;
            $scope.showSelectMonth = true;
            $scope.showSelectDate = false;
            $scope.showSelectWeek = false;
          } else {
            $('.loader-data').html('');
            $("#downloadLink").parent().attr('action', '/reports/generate_report?type=monthly')
            $scope.showLink = true;
            $scope.showSelectMonth = true;
            $scope.showSelectDate = false;
            $scope.showSelectWeek = false;
            $scope.totalItems = response.pagination.total_items;
            $scope.itemPerPage = response.pagination.per_page;
            $("#loader-container.monthly").hide();
          }
        }
      );
    }
  }

  $scope.getReportsByDate = function() {

    if ($("#report-date-to").val().length != 0){
      $scope.filterDateTo = $("#report-date-to").val();
    }
    if ($("#report-date-from").val().length != 0){
      $scope.filterDateFrom = $("#report-date-from").val();
    }

    $http.get("/reports/filter-by-params.json?date_from="+$scope.filterDateFrom+"&date_to="+$scope.filterDateTo+"&page="+$scope.currentPage).success(
      function(response){
        console.log(response);
        $scope.reports = response.data;

        if(response.data.length == 0){
          $scope.totalItems = 0;
          $('#loader-container').show();
          $('.loader-data').html('<p class="text-info">No reports found.</p>');
        }else{
          $scope.totalItems = response.pagination.total_items;
          $scope.itemPerPage = response.pagination.per_page;
          $('#loader-container').hide();
        }

      }
      );

  }

  $scope.getSelectedOptionReportsByDate = function() {

    if ($("#report-date-to").val().length != 0){
      $scope.filterDateTo = $("#report-date-to").val();
    }
    if ($("#report-date-from").val().length != 0){
      $scope.filterDateFrom = $("#report-date-from").val();
    }

    $http.get("/reports/option-selected.json?date_from="+$scope.filterDateFrom+"&date_to="+$scope.filterDateTo+"&page="+$scope.currentPage).success(
      function(response){
        console.log(response);
        $scope.reports = response.data;

        if(response.data.length == 0){
          $scope.totalItems = 0;
          $('#loader-container').show();
          $('.loader-data').html('<p class="text-info">No reports found.</p>');
        }else{
          $scope.totalItems = response.pagination.total_items;
          $scope.itemPerPage = response.pagination.per_page;
          $('#loader-container').hide();
        }

      }
      );

  }

  $scope.getSmsSentReportsByDate = function() {

      if ($("#report-date-to").val().length != 0){
        $scope.filterDateTo = $("#report-date-to").val();
      }
      if ($("#report-date-from").val().length != 0){
        $scope.filterDateFrom = $("#report-date-from").val();
      }

      $http.get("/reports/sms-sent-data.json?date_from="+$scope.filterDateFrom+"&date_to="+$scope.filterDateTo+"&page="+$scope.currentPage).success(
        function(response){
          console.log(response);
          $scope.reports = response.data;

          if(response.data.length == 0){
            $scope.totalItems = 0;
            $('#loader-container').show();
            $('.loader-data').html('<p class="text-info">No reports found.</p>');
          }else{
            $scope.totalItems = response.pagination.total_items;
            $scope.itemPerPage = response.pagination.per_page;
            $('#loader-container').hide();
          }

        }
        );
    }

    $scope.getSmsReceivedReportsByDate = function() {

      if ($("#report-date-to").val().length != 0){
        $scope.filterDateTo = $("#report-date-to").val();
      }
      if ($("#report-date-from").val().length != 0){
        $scope.filterDateFrom = $("#report-date-from").val();
      }

      $http.get("/reports/sms-received-data.json?date_from="+$scope.filterDateFrom+"&date_to="+$scope.filterDateTo+"&page="+$scope.currentPage).success(
        function(response){
          console.log(response);
          $scope.reports = response.data;

          if(response.data.length == 0){
            $scope.totalItems = 0;
            $('#loader-container').show();
            $('.loader-data').html('<p class="text-info">No reports found.</p>');
          }else{
            $scope.totalItems = response.pagination.total_items;
            $scope.itemPerPage = response.pagination.per_page;
            $('#loader-container').hide();
          }

        }
        );
    }

    $scope.removeTodo = function(data, index) {
      var deleteUser = confirm('Are you absolutely sure you want to delete?');

      if (deleteUser) {
        return $http.delete('/globelabs/delete?process_date='+ data).success(function(data) {
          $scope.reports.splice(index, 1);
        });
      }
    };

    $scope.getDateFromWeekNumber = function(year, week) {
      var dte = new Date(parseInt(year), 0, 0);
      var wk = parseInt(week) * 7;
      dte.setDate(dte.getDate() + wk);

      return dte;
    }
});
