'use strict';

/* Controllers */


//inject angular file upload directives and services.
var app = angular.module('fileUpload', ['ngFileUpload']);

app.controller('MyCtrl', ['$scope', 'Upload', function ($scope, Upload) {
    // $scope.$watch('files', function () {
    //     $scope.upload($scope.files);
    // });
    $scope.log = '';
    $scope.loading = false ;
    $scope.modal_title = "";
    $scope.modal_message = "";
    $scope.error_message = "";
    //Upload Voice
    $scope.upload = function (files) {
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                $scope.loading = true ; 
                console.log("running");
                Upload.upload({
                    url: '/upload-data',
                    fields: {
                        'blastingDate': $('#blastingDate').val()
                    },
                    file: file
                }).progress(function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    $scope.log = 'progress: ' + progressPercentage + '% ' +
                                evt.config.file.name + '\n' + $scope.log;
                }).success(function (data, status, headers, config) {
                    console.log(status);
                    $scope.modal_title = "Kopi VBB";
                    $scope.modal_message = data.response;
                    $('#modal-success').modal('show');
                    $scope.loading = false ;
                    // $scope.log = 'file ' + config.file.name + 'uploaded. Response: ' + JSON.stringify(data) + '\n' + $scope.log;
                    // // $scope.$apply();
                    // $scope.loading = false ; 
                }).error(function(data, status, headers, config){
                    console.log(status);
                    if (status == 500) {
                        var str = JSON.stringify(data).split("<h2>")[1].split("<").shift();
                        $scope.error_message = "Error found in " + str.split(/[()]/)[1] + ". Please check your file then try again.";
                    }
                    $scope.modal_title = "Sorry!";
                    $scope.modal_message = $scope.error_message;
                    $('#modal-success').modal('show');
                    $scope.loading = false ;
                //TODO: handle error
                });
            }
        }
    };
}]);