############################################################################################
# Original Description: Describe the content of this new Commercial SSL Package for ESB
#
# Author: Joemar Gealogo
# Version: 1.1
# Date: March 15, 2017 
# Modification History: 
#	March 20, 2017 : Replace CA chain with root CA as certificate to be installed in
#			the truststore. 
#			Change OneWaySSLClient usage
#
############################################################################################

# warsb.globe.com.ph.server.root.ca.pem
This the commercial root CA certificate. This should be installed in case the Application/Platform is using the default CA bundle installed in the OS.

# warsb.globe.com.ph.server.ca.chain.bundle.pem
This the commercial CA Chain certificate. For referrence

# warcraft-common-inbound-truststore.jks
This is the ready made trustore in case the Application/Platform used this truststore prior to this renewal.
Password must be requested from ESB Team and for Approval of ESB SLM.

# OneWaySSLClient.java
Stand alone JAVA application to simulate One Way SSL of this new commercial certificate.
Truststore in use is warcraft-common-inbound-truststore.jks
Compilation command:
	javac OneWaySSLClient.java

# OneWaySSLClient.class
Compiled standalone application of OneWaySSLClient.java using jdk1.6
Usage:
	java OneWaySSLClient <Context URI> warsb.globe.com.ph 443 <truststore password>

Example: 
	java OneWaySSLClient /sms?wsdl warsb.globe.com.ph 443 password
	
