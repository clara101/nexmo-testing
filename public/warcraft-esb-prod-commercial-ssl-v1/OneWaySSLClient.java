import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class OneWaySSLClient {
   
    public static void main(String[] args) {
      
        try{
         
            //set necessary truststore properties - using JKS
            System.setProperty("javax.net.ssl.trustStore","warcraft-common-inbound-truststore.jks");
            System.setProperty("javax.net.ssl.trustStorePassword",args[3]);
            // register a https protocol handler  - this may be required for previous JDK versions
            //System.setProperty("java.protocol.handler.pkgs","com.sun.net.ssl.internal.www.protocol");
          
			//debug ssl
            System.setProperty("javax.net.debug","ssl");            
            
            //connect to warcraft dev           
            SSLSocketFactory factory = (SSLSocketFactory) SSLSocketFactory.getDefault();            
            SSLSocket sslSock = (SSLSocket) factory.createSocket(args[1],Integer.parseInt(args[2]));
		String[] protocols={"TLSv1"};
		sslSock.setEnabledProtocols(protocols);            
            //String[] newProtocols = {"SSLv2"};
            //sslSock.setEnabledProtocols(newProtocols);            
            
            for(String s:sslSock.getEnabledProtocols()){
            	System.out.println("ssl.protocol: " + s);
            }
            
            //send HTTP get request
            BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(sslSock.getOutputStream(), "UTF8"));            
            wr.write("GET " + args[0] + " HTTP/1.1\r\nhost: "+ args[1] +"\r\n\r\n");
            wr.flush();
             
            // read response
            BufferedReader rd = new BufferedReader(new InputStreamReader(sslSock.getInputStream()));           
            String string = null;

            while ((string = rd.readLine()) != null) {
                System.out.println(string);
                System.out.flush();
            }
           
            rd.close();
            wr.close();
            // Close connection.
            sslSock.close();
           
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
